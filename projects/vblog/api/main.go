package main

import "gitee.com/lishibao6/go-project-v2/projects/vblog/api/cmd"

func main() {
	cmd.Execute()
}

// package main

// import (
// 	"context"
// 	"fmt"
// 	"os"
// 	"os/signal"
// 	"sync"
// 	"syscall"
// 	"time"

// 	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/conf"
// 	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/protocol"
// 	"github.com/infraboard/mcube/logger/zap"
// )

// var (
// 	confType string
// 	confFile string
// )

// func loadConfig() error {
// 	switch confType {
// 	case "env":
// 		return conf.LoadConfigFromEnv()
// 	case "file":
// 		return conf.LoadConfigFromToml(confFile)
// 	default:
// 		return fmt.Errorf("not supported config type, %s", confType)
// 	}
// }

// func loadGlobal() {
// 	// 全局日志对象
// 	zap.DevelopmentSetup()
// }

// func main() {

// 	//加载配置
// 	if err := loadConfig(); err != nil {
// 		fmt.Println(err)
// 	}

// 	// 初始化全局变量
// 	loadGlobal()

// 	// 需要监听来自os的信号，比如你取消了或者终止了服务
// 	ch := make(chan os.Signal, 1)

// 	// SIGTERM: 发送此信号请求进程终止。进程可以在终止之前执行清理任务。
// 	// SIGINT: 中断进程的信号。通常由用户在终端中按下Ctrl-C触发。
// 	// SIGHUP: 当进程的控制终端关闭或进程在后台运行且其终端被断开连接时，会发送此信号。
// 	// SIGQUIT: 类似于SIGTERM，但它请求进程终止并生成用于调试的核心转储。
// 	signal.Notify(ch, syscall.SIGTERM, syscall.SIGINT, syscall.SIGHUP, syscall.SIGQUIT)

// 	// http server启动时阻塞的
// 	http := protocol.NewHTTP()

// 	wg := &sync.WaitGroup{}
// 	wg.Add(1)
// 	go func() {
// 		// 多个Goroutine 同时执行的 有可能还没来得及+1, wg就退出了
// 		// wg.Add(1)
// 		defer wg.Done()

// 		// 启动一个Goroutine在后台，处理来自操作系统的信号
// 		for v := range ch {
// 			zap.L().Infof("receive signal: %s, stop service", v)

// 			//优雅关闭HTTP服务
// 			switch v {
// 			//kill -HUP <pid>可以发送HUP信号，通常用于重新读取配置文件
// 			case syscall.SIGHUP:
// 				if err := loadConfig(); err != nil {
// 					zap.L().Errorf("reload config error, %s", err)
// 				}
// 			default:
// 				// 优雅关闭HTTP服务
// 				ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
// 				defer cancel()
// 				http.Stop(ctx)
// 			}

// 			// 退出循环，Goroutine退出
// 			return
// 		}
// 	}()

// 	err := http.Start()
// 	if err != nil {
// 		fmt.Println(err)
// 	}

// 	// 等待程序优雅关闭完成
// 	wg.Wait()
// }

// func init() {
// 	conf.LoadConfigFromToml("./etc/config.toml")
// }
