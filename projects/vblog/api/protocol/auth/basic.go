package auth

import (
	"net/http"

	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/conf"
	"github.com/gin-gonic/gin"
	"github.com/infraboard/mcube/logger/zap"
)

func BasicAuth(c *gin.Context) {
	// 处理请求:  检查BasicAuth: Basic YWRtaW46MTIzNDU2
	// username:password
	zap.L().Debugf("basic auth: %s", c.Request.Header.Get("Authorization"))

	// 1.获取用户的用户密码
	username, password, ok := c.Request.BasicAuth()
	if !ok {
		// AbortWithStatusJSON 用于终止请求并返回带有指定状态码和 JSON 数据的错误响应
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"code":    http.StatusUnauthorized,
			"message": "auth required",
		})
		return
	}

	// 和系统配置的用户密码进行比对
	zap.L().Debugf("auth user: %s", username)
	ac := conf.GetConfig().Auth
	if !(username == ac.Username && password == ac.Password) {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"code":    http.StatusUnauthorized,
			"message": "Incorrect username or password",
		})
		return
	}

	// 处理响应: 无

	// 继续路由到后面的Handler
	c.Next()
}
