package protocol

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps"
	_ "gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/all"
	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/conf"
	"github.com/gin-gonic/gin"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

// HTTP 服务， 需要有简单的地址和端口，从全局配置中获取Server监听参数
func NewHTTP() *HTTP {
	r := gin.Default()
	return &HTTP{
		log:    zap.L().Named("server.http"),
		router: r,
		server: &http.Server{
			ReadHeaderTimeout: 60 * time.Second,
			ReadTimeout:       60 * time.Second,
			WriteTimeout:      60 * time.Second,
			IdleTimeout:       60 * time.Second,
			MaxHeaderBytes:    1 << 20, //1M
			// 处理HTTP 协议的逻辑,HTTP 中间件，是一个路由(框架,Gin,...)与处理(自己)
			Handler: r,
			Addr:    conf.GetConfig().App.HTTP.Addr(),
		},
	}
}

// HTTP 服务对象
type HTTP struct {
	router *gin.Engine
	server *http.Server
	log    logger.Logger
}

func (h *HTTP) Start() error {
	err := apps.Init()
	if err != nil {
		return err
	}

	v1 := h.router.Group("/vblog/api/v1")

	apps.InitHttpService(v1)

	// 打印下日志
	h.log.Infof("http server listen on: %s", h.server.Addr)

	err = h.server.ListenAndServe()
	if err != nil {
		// 处理正常退出情况
		if err == http.ErrServerClosed {
			return nil
		}
		return fmt.Errorf("server ListenAndServe error, %s", err)
	}
	return nil
}

func (h *HTTP) Stop(ctx context.Context) {
	h.log.Infof("server graceful shutdown ....")
	// HTTP Server 优雅关闭
	// 支持ctx, 10分 请求都没退出, 做超时设置
	err := h.server.Shutdown(ctx)
	if err != nil {
		h.log.Warnf("shutdown error, %s", err)
	} else {
		h.log.Infof("server graceful shutdown ok")
	}
}
