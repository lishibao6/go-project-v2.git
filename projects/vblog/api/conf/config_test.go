package conf_test

import (
	"testing"

	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/conf"
)

// 测试用例如何获取环境变量
// 测试用例的执行，时vscode提供的, 因此vscode为测试用例提供的加载环境变量的设置
// 通过vscode的设置:  "go.testEnvFile": "${workspaceFolder}/etc/unit_test.env"
// 表示测试用例执行时，加载的环境文件: "${workspaceFolder}/etc/unit_test.env"
// 需要符合vscode解析的格式: key=value

func TestLoadConfigFromEnv(t *testing.T) {
	err := conf.LoadConfigFromEnv()
	if err != nil {
		t.Fatal(err)
	}
	t.Log(conf.GetConfig())
}

func TestLoadConfigFromToml(t *testing.T) {
	err := conf.LoadConfigFromToml("../etc/config.toml")
	if err != nil {
		t.Fatal(err)
	}
	t.Log(conf.GetConfig())
}
