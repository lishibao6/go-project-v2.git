# 程序配置对象管理

## 定义项目配置对象

```go
// 程序所有配置信息都保存再该对象上
// Struct Tag: toml, "github.com/BurntSushi/toml"解析时 完成配置文件<-->配置对象的映射
// Struct Tag: env, "github.com/caarlos0/env/v6"解析时 完成环境变量<-->配置对象的映射
type Config struct {
	App   *app   `toml:"app" json:"app"`
	MySQL *mysql `toml:"mysql" json:"mysql"`
	Auth  *auth  `toml:"auth" json:"auth"`
}
```

## 管理配置对象上的单例对象

```go
type mysql struct {
	Host     string `toml:"host" json:"host" env:"MYSQL_HOST"`
	Port     string `toml:"port" json:"port" env:"MYSQL_PORT"`
	Database string `toml:"database" json:"database" env:"MYSQL_DATABASE"`
	Username string `toml:"username" json:"username" env:"MYSQL_USERNAME"`
	Password string `toml:"password" json:"password" env:"MYSQL_PASSWORD"`

	// 连接池设置
	// 最大连接数
	MaxOpenConn int `toml:"max_open_conn" json:"max_open_conn" env:"MYSQL_MAX_OPEN_CONN"`
	// 最大的最大闲置连接数
	MaxIdleConn int `toml:"max_idel_conn" json:"max_idel_conn" env:"MYSQL_MAX_IDLE_CONN"`
	// 连接的有效时间, 小于服务端的设置时间
	MaxLifeTime int `toml:"max_life_time" json:"max_life_time" env:"MYSQL_MAX_LIFE_TIME"`
	// 一个闲置的连接多久没用会被释放
	MaxIdleTime int `toml:"max_idel_time" json:"max_idel_time" env:"MYSQL_MAX_IDEL_TIME"`

	lock   sync.Mutex
	dbconn *sql.DB
	orm    *gorm.DB
}

func (m *mysql) Dsn() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&multiStatements=true",
		m.Username,
		m.Password,
		m.Host,
		m.Port,
		m.Database,
	)
}

func (m *mysql) GetORMDB() *gorm.DB {
	m.lock.Lock()
	defer m.lock.Unlock()

	if m.orm == nil {
		db, err := gorm.Open(gormmysql.Open(m.Dsn()))
		if err != nil {
			panic(err)
		}
		m.orm = db
	}

	return m.orm
}
```

## 配置对象默认值初始化

当你的程序没有配置文件时，也能有默认值，并且可以正常运行

```go
func NewDefaultConfig() *Config {
	return &Config{
		App:   newDefautApp(),
		MySQL: newDefautMySQL(),
		Auth:  &auth{},
	}
}

func newDefautMySQL() *mysql {
	return &mysql{
		Host:     "localhost",
		Port:     "3306",
		Database: "vblog",
		Username: "root",
		Password: "123456",
	}
}
```

## 配置对象实现全局单例

```go
var (
	// 为了保护该变量不被外部修改, 未暴露
	config *Config
)

// 单独提供一个方式 Getter
func C() *Config {
	if config == nil {
		panic("load config first")
	}

	return config
}
```

## 配置对象加载

```go
// 从环境变量中加载配置, 加载成 一个全局变量
func LoadConfigFromEnv() error {
	config = NewDefaultConfig()
	return env.Parse(config)
}

func LoadConfigFromToml(filepath string) error {
	config = NewDefaultConfig()
	_, err := toml.DecodeFile(filepath, config)
	if err != nil {
		return err
	}
	return nil
}
```