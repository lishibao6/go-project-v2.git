package cmd

import (
	"fmt"
	"os"

	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/version"
	"github.com/spf13/cobra"
)

var (
	vers bool
)

// RootCmd 表示在没有任何子命令的情况下调用时的基本命令
var RootCmd = &cobra.Command{
	Use:   "vblog",
	Short: "微博客系统后台API Server",
	Long:  "微博客系统后台API Server",
	// 执行vblog -v会打印版本信息
	// 其他情况执行cmd.Help(),打印帮助信息
	RunE: func(cmd *cobra.Command, args []string) error {
		if vers {
			fmt.Println(version.FullVersion())
			return nil
		}
		return cmd.Help()
	},
}

// Execute 将所有子命令添加到根命令中并适当地设置标志。
// 这由 main.main() 调用。 它只需要对 rootCmd 发生一次。
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	// 为RootCmd添加Flags
	RootCmd.PersistentFlags().BoolVarP(&vers, "version", "v", false, "the vblog api version")

	// RootCmd.PersistentFlags().StringVarP(&confType, "config-type", "t", "file", "the service config type [file/env/etcd]")
	// RootCmd.PersistentFlags().StringVarP(&confFile, "config-file", "f", "etc/config.toml", "the service config from file")
	// RootCmd.PersistentFlags().StringVarP(&confETCD, "config-etcd", "e", "127.0.0.1:2379", "the service config from etcd")
}
