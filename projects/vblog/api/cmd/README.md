# 程序CLI

.vblog-api start -t file -f etc/config.toml

<!-- ./vblog-api  -->

## cobra 

命令行的CLI库, 用于编写CLI风格的命令

## 测试

### 编译

```Bash
#进入代码所在目录gitee.com/lishibao6/go-project-v2/projects/vblog/api
make build
```

### ./dist/vblog 
```Bash
微博客系统后台API Server

Usage:
  vblog [flags]
  vblog [command]

Available Commands:
  completion  Generate the autocompletion script for the specified shell
  help        Help about any command
  start       启动 API Server

Flags:
  -h, --help      help for vblog
  -v, --version   the vblog api version

Use "vblog [command] --help" for more information about a command.
```

### ./dist/vblog -v
```Bash
Version : 0.0.1
Build Time: 2023-05-26 15:42:14
Git Branch: master
Git Commit: f0c1072b3367c7bfd895e2ce5b0344cbf1079adf
Go Version: go1.20.1 windows/amd64
```

### ./dist/vblog start -h
```Bash
启动 API Server

Usage:
  vblog start [flags]

Flags:
  -e, --config-etcd string   the service config from etcd (default "127.0.0.1:2379")
  -f, --config-file string   the service config from file (default "etc/config.toml")
  -t, --config-type string   the service config type [file/env/etcd] (default "file")
  -h, --help                 help for start

Global Flags:
  -v, --version   the vblog api version
```

## 启动
```Bash
./dist/vblog start -t file etc/config.toml
```
