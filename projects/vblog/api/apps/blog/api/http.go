package api

import (
	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps"
	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/blog"
	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/tag"
	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/protocol/auth"
	"github.com/gin-gonic/gin"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

func NewHTTPAPI() *HTTPAPI {
	return &HTTPAPI{}
}

// HTTPAPI 定义用来 对外暴露HTTP服务，注册给协议Server(HTTP Server, Gin)
type HTTPAPI struct {
	// 业务逻辑： 后端Blog Service的实现
	service blog.Service
	tagSvc  tag.Service
	// 打印日志 log.Print, logrus, zap(Uber高性能)
	// 定义了Logger接口, 可以容纳多种Log库的实现
	// 选择使用 原生的logrus 或者 zap 或者直接使用 log
	// 替换成标准逻辑: https://pkg.go.dev/go.uber.org/zap
	log logger.Logger
}

// 把HTTP API托管给IOC
// init时获取依赖注入
func (h *HTTPAPI) Init() error {
	h.service = apps.GetService(blog.AppName).(blog.Service)
	h.tagSvc = apps.GetService(tag.AppName).(tag.Service)
	h.log = zap.L().Named("api.blog")
	return nil
}

func (h *HTTPAPI) Name() string {
	return blog.AppName
}

// URI 注册给Gin
func (h *HTTPAPI) Registry(r gin.IRouter) {
	// 开发接口 不需要认证
	r.GET("/", h.QueryBlog)
	r.GET("/:id", h.DescribeBlog)

	// 管理员的接口, 需要认证
	r.Use(auth.BasicAuth)
	r.POST("/", h.CreateBlog)
	r.DELETE("/:id", h.DeleteBlog)
	r.PUT("/:id", h.PutBlog)
	r.PATCH("/:id", h.PatchBlog)
	r.POST("/:id/status", h.UpdateBlogStatus)
}

func init() {
	apps.RegistryHttp(&HTTPAPI{})
}
