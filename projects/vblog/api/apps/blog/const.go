package blog

import (
	"bytes"
	"fmt"
	"strings"
)

type Status int

//不进行序列化的话，用户前端看到的是: "status": 0
//序列化之后，用户前端看到的是: "status": "draft"

// Json允许用户自定义序列化和反序列化的逻辑
// 控制序列化的过程 ---> draft  {"status": "draft"}
// Status的序列化和反序列化函数不需要手动调用，
// 在通过url访问的时候，会自动调用Status的序列化和反序列化函数
func (s Status) MarshalJSON() ([]byte, error) {
	// 枚举为啥要全部转化为小写 Draft DRAFT draft
	// 字符串拼接 "draft"
	b := bytes.NewBufferString(`"`)
	b.WriteString(strings.ToLower(s.String()))
	b.WriteString(`"`)
	return []byte(b.String()), nil
}

// // {"status": 1}
// // 定义类型的反序列化 {"status": "published"}
func (s *Status) UnmarshalJSON(b []byte) error {
	ins, err := ParseStatusFromString(string(b))
	if err != nil {
		return err
	}
	// 需要把当前的Status赋值
	*s = ins
	return nil
}

// ParseTagTypeFromString Parse StatusType from string: "draft"
func ParseStatusFromString(str string) (Status, error) {
	// 去掉stru收尾的"
	value := strings.Trim(str, `"`)
	for k, v := range STATUS_MAP {
		if v == value {
			return k, nil
		}
	}

	return Status(-1), fmt.Errorf("unknow Status: %s", str)
}

const (
	// 草稿
	STATUS_DRAFT Status = 0
	// 已发布
	STATUS_PUBLISHED Status = 1
)

// Status 实现fmt Stringer接口
func (s Status) String() string {
	if v, ok := STATUS_MAP[s]; ok {
		return v
	}
	return fmt.Sprintf("%d", s)
}

var (
	STATUS_MAP = map[Status]string{
		STATUS_DRAFT:     "draft",
		STATUS_PUBLISHED: "published",
	}
)

// 更新模式
type UpdateMode string

const (
	// 全量更新
	UPDATE_MODE_PUT UpdateMode = "put"
	// 部分更新
	UPDATE_MODE_PATCH UpdateMode = "patch"
)
