// 将信息入库
package impl

import (
	"context"

	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/blog"
)

// 对象的保存
// 1. 采用原生的SQL, INSERT INTO table_name (filed_name,...) VALUES(filed_value,...)
// 2. 采用GORM：https://gorm.io/docs/
// context, 网络比较慢, 用户登录3秒，用户取消, 数据库的操作也必须支持需求,

func (i *Impl) save(ctx context.Context, ins *blog.Blog) error {
	return i.DB().WithContext(ctx).Create(ins).Error
}
