package impl

import (
	"context"
	"time"

	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/blog"
	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/tag"
	"github.com/imdario/mergo"
	"github.com/infraboard/mcube/exception"
)

// 创建文章
func (i *Impl) CreateBlog(ctx context.Context, req *blog.CreateBlogRequest) (*blog.Blog, error) {
	//校验对象的合法性
	err := req.Validate()
	if err != nil {
		// 工程是否需要定义统一异常, 不定义 都可以字符串的形式返回
		// errors.New("validate create blog request error, " + err.Error())
		return nil, exception.NewBadRequest("validate create blog request, %s", err)
	}

	//创建对象
	ins := blog.NewCreateBlog(req)

	// 把对象入库, id 是自增主键, 是由数据库计算
	// LastId()  insert
	// ins.Id = LastId()
	err = i.save(ctx, ins)
	if err != nil {
		return nil, err
	}

	return ins, nil
}

// 文章列表
func (i *Impl) QueryBlog(ctx context.Context, req *blog.QueryBlogRequest) (*blog.BlogSet, error) {
	// time.Sleep(3 * time.Second)
	blogSet := blog.NewBlogSet()

	query := i.DB()

	// 条件拼接
	// 支持关键字查询,  .*
	if req.Keywords != "" {
		query = query.Where(
			"title_name LIKE ? OR content LIKE ?",
			"%"+req.Keywords+"%",
			"%"+req.Keywords+"%",
		)
	}

	// 状态过滤
	if req.Status != nil {
		query = query.Where("status = ?", *req.Status)
	}

	//如何查询总条数:COUNT(*)
	err := query.Count(&blogSet.Total).Error
	if err != nil {
		return nil, err
	}

	// 需要查询分页数据
	// SQL 如何支持分页 LIMIT <offset>,<limit>
	// 并且倒序排列，让最新创建的条目放到最前面
	query = query.Offset(req.Offset()).Limit(req.PageSize).Order("create_at DESC")
	err = query.WithContext(ctx).Scan(&blogSet.Items).Error
	if err != nil {
		return nil, err
	}

	// 补充Tag标签,为查询所有Blog都补充标签
	for index := range blogSet.Items {
		blog := blogSet.Items[index]
		queryTagReq := tag.NewQueryTagRequest()
		tagSet, err := i.tag.QueryTag(ctx, queryTagReq)
		if err != nil {
			return nil, err
		}
		blog.Tags = tagSet.Items
	}

	return blogSet, nil
}

// 文章详情
func (i *Impl) DescribeBlog(ctx context.Context, req *blog.DescribeBlogRequest) (*blog.Blog, error) {
	// 默认blog实例
	ins := blog.NewCreateBlog(blog.NewCreateBlogRequest())

	query := i.DB().Where("id = ?", req.Id)
	err := query.Find(ins).Error
	if err != nil {
		return nil, err
	}

	// 注意处理404
	//在使用Go语言的GORM框架进行查询时，如果MySQL数据库中没有指定id的记录，
	//则查询结果中的id字段将保持默认值0。
	//这是因为GORM在查询时会将结果映射到指定的结构体中，
	//而结构体的未初始化字段的默认值为0或空值。
	if ins.Id == 0 {
		return nil, exception.NewNotFound("blog %d not found", req.Id)
	}

	return ins, nil
}

// 更新文章
func (i *Impl) UpdateBlog(ctx context.Context, req *blog.UpdateBlogRequest) (*blog.Blog, error) {
	//返回数据库中的blog
	ins, err := i.DescribeBlog(ctx, blog.NewDescribeBlogRequest(req.Id))
	if err != nil {
		return nil, err
	}

	switch req.UpdateMode {
	case blog.UPDATE_MODE_PUT:
		// 对象全量更新
		ins.CreateBlogRequest = req.CreateBlogRequest
	case blog.UPDATE_MODE_PATCH:
		// 局部更新, 有很多要更新的字段
		// ins.CreateBlogRequest.TitleName = req.TitleName
		// ins.CreateBlogRequest.Content = req.Content
		// 有没有 能进行 object merge lib: "github.com/imdario/mergo"
		// PATCH  old <---- new old  有更新就更新，没有更新就不更新
		err := mergo.MergeWithOverwrite(ins.CreateBlogRequest, req.CreateBlogRequest)
		if err != nil {
			return nil, err
		}
	default:
		return nil, exception.NewBadRequest("update mode not support %s", req.UpdateMode)
	}

	// 实例对象更新完成
	// 检查update参数的合法性,需要入库之前检查,因为有Patch操作
	err = ins.CreateBlogRequest.Validate()
	if err != nil {
		return nil, exception.NewBadRequest("validate request error, %s", err)
	}

	// 数据库更新
	// Updates: 更新已经存在的记录
	// Save: 创建新的记录或更新已存在的记录，根据对象的主键进行判断
	// err = i.DB().WithContext(ctx).Updates(ins).Error
	err = i.DB().WithContext(ctx).Save(ins).Error
	if err != nil {
		return nil, err
	}

	return ins, nil
}

// 文章的删除
// 为什么删除后，还要返回数据, 方便前端和事件总线使用
func (i *Impl) DeleteBlog(ctx context.Context, req *blog.DeleteBlogRequest) (*blog.Blog, error) {
	// delete的时候 需要返回blog对象, 先查询blog
	// time.Sleep(3 * time.Second)
	ins, err := i.DescribeBlog(ctx, blog.NewDescribeBlogRequest(req.Id))
	if err != nil {
		return nil, err
	}

	err = i.DB().Delete(ins).Error
	if err != nil {
		return nil, err
	}

	return ins, nil
}

// 更新文章状态
func (i *Impl) UpdateBlogStatus(ctx context.Context, req *blog.UpdateBlogStatusRequest) (*blog.Blog, error) {
	// 需要判断该对象是否存在， SQL EXIST

	//根据blog id获取blog对象
	ins, err := i.DescribeBlog(ctx, blog.NewDescribeBlogRequest(req.Id))
	if err != nil {
		return nil, err
	}

	//判断状态是否一致
	if ins.Status == req.Status {
		return nil, exception.NewBadRequest("status already %s,", req.Status)
	}

	//状态不一致则进行更新
	ins.Status = req.Status
	if ins.Status == blog.STATUS_PUBLISHED {
		ins.PublishAt = time.Now().Unix()
	}

	// 入库保存
	err = i.DB().WithContext(ctx).Save(ins).Error
	if err != nil {
		return nil, err
	}

	return ins, nil
}
