package impl

import (
	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps"
	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/tag"
	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/conf"
	"gorm.io/gorm"
)

func NewImpl() *Impl {
	return &Impl{}
}

// 负责实现Blog Service
type Impl struct {
	db  *gorm.DB
	tag tag.Service
}

func (i *Impl) Name() string {
	return "blog"
}

func (i *Impl) DB() *gorm.DB {
	return i.db.Table(i.Name())
}

// 当这个对象初始化的，会获取该对象需要的依赖
// 需要db这个依赖, 从配置文件中获取
func (i *Impl) Init() error {
	i.db = conf.GetConfig().MySQL.GetORMDB().Debug()
	i.tag = apps.GetService(tag.AppName).(tag.Service)
	return nil
}

// import _ "gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/blog/impl"
func init() {
	apps.Registry(&Impl{})
}
