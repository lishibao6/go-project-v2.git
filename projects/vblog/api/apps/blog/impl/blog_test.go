package impl_test

import (
	"context"
	"testing"

	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps"
	_ "gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/all"
	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/blog"
	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/conf"
	"github.com/infraboard/mcube/exception"
	"github.com/stretchr/testify/assert"
)

var blogService blog.Service

// vscode 用户设置 搜索: testFlag, 添加go testFlag插件配置
// "go.testFlags": [
// 	"-count=1",
// 	"-v"
// ]
// -v: 打印测试用例Debug信息, t.Log以及fmt.Print() 的信息
// -count=1: 如果你的用例没有修改, 默认直接使用上次测试运行的结构(不会真的再执行一次, Cached)

func TestValidateBlog(t *testing.T) {
	req := blog.NewCreateBlogRequest()
	ins := blog.NewCreateBlog(req)
	err := ins.CreateBlogRequest.Validate()
	if err != nil {
		t.Fatal(err)
	}
}

func TestCreateBlog(t *testing.T) {
	req := blog.NewCreateBlogRequest()
	req.TitleName = "Vblog11"
	req.Summary = "文章概要"
	req.Content = "develop vblog system"
	req.Author = "lisi"
	ins, err := blogService.CreateBlog(context.Background(), req)
	if err != nil {
		if v, ok := err.(exception.APIException); ok {
			t.Log(v.ErrorCode())
		}
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestQueryBlog(t *testing.T) {
	req := blog.NewQueryBlogRequest()
	req.Keywords = "blog"
	ins, err := blogService.QueryBlog(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestDescribeBlog(t *testing.T) {
	req := blog.NewDescribeBlogRequest(1)
	ins, err := blogService.DescribeBlog(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestUpdateBlog(t *testing.T) {
	req := blog.NewPutUpdateBlogRequest(9)
	req.TitleName = "xxxx"
	req.Content = "测试更新"
	req.TitleImg = "img_url"
	ins, err := blogService.UpdateBlog(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestPatchBlog(t *testing.T) {
	req := blog.NewPatchUpdateBlogRequest(1)
	req.TitleImg = "更新URL"
	ins, err := blogService.UpdateBlog(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(ins)
}

func TestDeleteBlog(t *testing.T) {
	req := blog.NewDeleteBlogRequest(1)
	ins, err := blogService.DeleteBlog(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestUpdateBlogStatus(t *testing.T) {
	req := blog.NewUpdateBlogStatusRequest(1, blog.STATUS_PUBLISHED)
	ins, err := blogService.UpdateBlogStatus(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}

	should := assert.New(t)
	if should.Equal(blog.STATUS_PUBLISHED, ins.Status) {
		t.Log(ins)
	}
}

func init() {
	// 加载配置
	err := conf.LoadConfigFromEnv()
	if err != nil {
		panic(err)
	}

	// // 具体实现
	// svc := impl.NewImpl()
	// //初始化
	// err = svc.Init()
	// if err != nil {
	// 	panic(err)
	// }
	err = apps.Init()
	if err != nil {
		panic(err)
	}
	blogService = apps.GetService(blog.AppName).(blog.Service)
}
