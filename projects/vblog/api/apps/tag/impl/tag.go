package impl

import (
	"context"
	"fmt"

	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/blog"
	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/tag"
	"github.com/infraboard/mcube/exception"
)

// 文章添加Tag
func (i *Impl) AddTag(ctx context.Context, req *tag.AddTagRequest) (
	*tag.TagSet, error) {
	// 参数校验<只校验的参数的有无>
	err := req.Validate()
	if err != nil {
		return nil, exception.NewBadRequest("validate AddTagRequest error, %s", err)
	}

	// 校验对象是否存在<blog_id>对象的blog是否真的存在
	// 性能的问题的写法
	bids := req.BlogIds()
	for _, bid := range bids {
		// 20 bid是不是就要数据交互20次
		_, err := i.blog.DescribeBlog(ctx, blog.NewDescribeBlogRequest(bid))
		if err != nil {
			return nil, err
		}
	}

	// 构造对象
	tagSet := tag.NewTagSetFromAddTagRequest(req)
	// err = i.DB().Save(tagSet.Items).Error  //Save如果有就更新，没有会创建
	err = i.DB().Create(tagSet.Items).Error // 只执行Insert操作
	if err != nil {
		return nil, err
	}

	return tagSet, nil
}

// 根据TagId查询 Tag的列表
func (i *Impl) QueryTag(ctx context.Context, req *tag.QueryTagRequest) (*tag.TagSet, error) {
	tagSet := tag.NewTagSet()

	query := i.DB()

	// 根据tag表中的blog_id字段查询tag
	// BlogId!=0表示传入了BlogId
	if req.BlogId != 0 {
		query = query.Where("blog_id = ?", req.BlogId)
	}

	// 或者根据tagId查询tag
	if len(req.TagIds) > 0 {
		// SELECT * FROM tag WHERE id IN (1,2);
		query = i.DB().Where("id in ?", req.TagIds)
	}

	err := query.Scan(&tagSet.Items).Error
	if err != nil {
		return nil, err
	}

	return tagSet, nil
}

// 文章移除Tag
func (i *Impl) RemoveTag(ctx context.Context, req *tag.RemoveTagRequest) (*tag.TagSet, error) {
	queryReq := tag.NewQueryTagRequest()
	queryReq.AddTagId(req.TagIds...)
	//从数据库中查询tag
	tagSet, err := i.QueryTag(ctx, queryReq)
	if err != nil {
		return nil, err
	}

	// 数据库中查询的tag(tagSet.Items)不等于用户要删除的tag(req.TagIds)
	if len(tagSet.Items) != len(req.TagIds) {
		// 要删除的多个tag中有tag不存在
		return nil, fmt.Errorf("has tag not exits")
	}

	// 删除这些Tag
	err = i.DB().Delete(tagSet.Items).Error
	if err != nil {
		return nil, err
	}

	return tagSet, nil
}
