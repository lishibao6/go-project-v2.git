package impl_test

import (
	"context"
	"testing"

	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps"
	_ "gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/all"
	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/tag"
	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/conf"
	"github.com/infraboard/mcube/logger/zap"
)

// tagSvc实现tag.Service接口
var tagSvc tag.Service

func TestAddTag(t *testing.T) {
	req := tag.NewAddTagRequest()
	createTagReq := tag.CreateTagRequest{
		BlogId: 1,
		Key:    "langurage",
		Value:  "Golang",
	}
	// createTagReq := tag.CreateTagRequest{
	// 	BlogId: 1,
	// 	Key:    "langurage",
	// 	Value:  "Python",
	// }
	// createTagReq := tag.CreateTagRequest{
	// 	BlogId: 2,
	// 	Key:    "langurage",
	// 	Value:  "C",
	// }
	req.AddCreateTag(&createTagReq)
	tagSet, err := tagSvc.AddTag(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tagSet)
}

// 测试根据BlogId查询tag
func TestQueryTagByBlogId(t *testing.T) {
	req := tag.NewQueryTagRequest()
	req.BlogId = 5
	tagSet, err := tagSvc.QueryTag(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tagSet)
}

// 测试根据TagIds查询tag
func TestQueryTagByTagIds(t *testing.T) {
	req := tag.NewQueryTagRequest()
	req.TagIds = []int{2, 3}
	tagSet, err := tagSvc.QueryTag(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tagSet)
}

func TestRemoveTag(t *testing.T) {
	req := tag.NewRemoveTagRequest()
	req.AddTagId(1)
	// req.AddTagId(1, 2) // 如果tagId为1的tag不存在，则会报错"has tag not exits"
	// req.AddTagId(2, 3)
	tagSet, err := tagSvc.RemoveTag(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tagSet)
}

func init() {
	err := conf.LoadConfigFromEnv()
	if err != nil {
		panic(err)
	}

	zap.DevelopmentSetup()

	// blog service
	// blogSvc := blogImpl.NewImpl()
	// err = blogSvc.Init()
	// if err != nil {
	// 	panic(err)
	// }

	// tag service
	// tagSvc = tagImpl.NewImpl(blogSvc)
	// err = tagSvc.Init()
	// if err != nil {
	// 	panic(err)
	// }

	err = apps.Init()
	if err != nil {
		panic(err)
	}

	tagSvc = apps.GetService(tag.AppName).(tag.Service)

}
