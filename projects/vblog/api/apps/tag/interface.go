package tag

import (
	"context"

	"github.com/go-playground/validator/v10"
)

const (
	AppName = "tag"
)

var (
	// 使用参考: https://raw.githubusercontent.com/go-playground/validator/master/_examples/simple/main.go
	validate = validator.New()
)

type Service interface {
	// 查询标签
	QueryTag(context.Context, *QueryTagRequest) (*TagSet, error)
	// 文章添加Tag
	AddTag(context.Context, *AddTagRequest) (*TagSet, error)
	// 文章移除Tag
	RemoveTag(context.Context, *RemoveTagRequest) (*TagSet, error)
}
