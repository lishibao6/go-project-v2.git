package tag

import (
	"encoding/json"
	"time"
)

func NewTagSet() *TagSet {
	return &TagSet{
		Items: []*Tag{},
	}
}

// Tag列表
type TagSet struct {
	Items []*Tag `json:"items"`
}

func (s *TagSet) String() string {
	dj, _ := json.Marshal(s)
	return string(dj)
}

// 添加tag到Items中
func (s *TagSet) Add(item *Tag) {
	s.Items = append(s.Items, item)
}

// 完整的tag结构体
// 包括业务数据和非业务数据
type Tag struct {
	//标签Id，由数据库通过自增主键生成
	Id int `json:"id"`
	//创建时间： 用于排序
	CreateAt int64 `json:"create_at"`
	// Tag的具体数据
	*CreateTagRequest
}

// 业务数据tag结构体
type CreateTagRequest struct {
	//关联的博客,同一个标签，允许打在不同的博客上
	BlogId int `json:"blog_id" validate:"required"`
	//标签名称
	Key string `json:"key" validate:"required"`
	// 标签的value
	Value string `json:"value" validate:"required"`
	// 标签的颜色
	Color string `json:"color"`
}

// 根据AddTagRequest创建tag，并返回一个tag切片
func NewTagSetFromAddTagRequest(req *AddTagRequest) *TagSet {
	// 创建一个空的tag切片
	tagSet := NewTagSet()
	for i := range req.CreateTags {
		createTag := req.CreateTags[i]
		// 创建一个新的tag
		tag := NewTag(createTag)
		// 添加tag到tag切片中
		tagSet.Add(tag)
	}
	return tagSet
}

// 创建一个新Tag
func NewTag(req *CreateTagRequest) *Tag {
	return &Tag{
		CreateAt:         time.Now().Unix(),
		CreateTagRequest: req,
	}
}

// 创建一个空的AddTagRequest
func NewAddTagRequest() *AddTagRequest {
	return &AddTagRequest{
		CreateTags: []*CreateTagRequest{},
	}
}

// 添加tag请求，一次可以添加多个tag
type AddTagRequest struct {
	// 一次可以添加多个Tag
	CreateTags []*CreateTagRequest
}

// 完成博客id去重
func (req *AddTagRequest) BlogIds() (bids []int) {
	bidMap := map[int]struct{}{}
	// 完成blog id的去重
	for i := range req.CreateTags {
		bid := req.CreateTags[i].BlogId
		bidMap[bid] = struct{}{}
	}

	// 把博客id添加到bids切片中
	for bid := range bidMap {
		bids = append(bids, bid)
	}
	return
}

// 添加tag到AddTagRequest的Tags切片中
func (req *AddTagRequest) AddCreateTag(tag *CreateTagRequest) {
	req.CreateTags = append(req.CreateTags, tag)
}

// 校验结构体的合法性
func (req *AddTagRequest) Validate() error {
	// for i := range req.Tags {
	// 	validate.Struct(req.Tags[i])
	// }
	return validate.Struct(req)
}

// 创建空的RemoveTagRequest结构体
func NewRemoveTagRequest() *RemoveTagRequest {
	return &RemoveTagRequest{
		TagIds: []int{},
	}
}

// 关于删除tag的结构体
type RemoveTagRequest struct {
	TagIds []int `json:"tag_ids"`
}

// 添加TagId到RemoveTagRequest结构体的TagIds切片中
func (req *RemoveTagRequest) AddTagId(ids ...int) {
	req.TagIds = append(req.TagIds, ids...)
}

// 创建一个空的QueryTagRequest结构体
func NewQueryTagRequest() *QueryTagRequest {
	return &QueryTagRequest{
		TagIds: []int{},
	}
}

// 关于查询Tag的结构体
type QueryTagRequest struct {
	// 博客关联的tagIds
	TagIds []int `json:"tag_ids"`
	// 博客ID
	BlogId int `json:"blog_id"`
}

// 添加TagId到TagIds
func (req *QueryTagRequest) AddTagId(ids ...int) {
	req.TagIds = append(req.TagIds, ids...)
}
