package api

import (
	"net/http"
	"strconv"

	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/tag"
	"github.com/gin-gonic/gin"
)

func (h *HTTPAPI) AddTag(c *gin.Context) {
	// 接收请求参数
	req := tag.NewAddTagRequest()
	err := c.BindJSON(&req.CreateTags)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code":    http.StatusBadRequest,
			"message": err.Error(),
		})
		return
	}

	tagSet, err := h.service.AddTag(c.Request.Context(), req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"code":    http.StatusInternalServerError,
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, tagSet)
}

func (h *HTTPAPI) RemoveTag(c *gin.Context) {
	// 接收请求参数
	req := tag.NewRemoveTagRequest()
	tagIdStr := c.Param("id")
	tagId, err := strconv.Atoi(tagIdStr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code":    http.StatusBadRequest,
			"message": err.Error(),
		})
		return
	}

	req.TagIds = []int{tagId}
	tagSet, err := h.service.RemoveTag(c.Request.Context(), req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"code":    http.StatusInternalServerError,
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, tagSet)
}
