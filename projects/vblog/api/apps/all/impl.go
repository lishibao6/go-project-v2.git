package all

// 注册服务实例对象
import (
	_ "gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/blog/impl"
	_ "gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/tag/impl"

	_ "gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/blog/api"
	_ "gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/tag/api"
	_ "gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/user/api"
)
