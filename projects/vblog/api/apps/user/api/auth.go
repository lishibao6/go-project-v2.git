package api

import (
	"net/http"

	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/user"
	"github.com/gin-gonic/gin"
)

func (h *HTTPAPI) Auth(c *gin.Context) {
	// 用户参数
	req := user.NewAuthRequest()
	err := c.BindJSON(req)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code":    http.StatusBadRequest,
			"message": err.Error(),
		})
		return
	}

	if req.Username == h.conf.Auth.Username && req.Password == h.conf.Auth.Password {
		c.JSON(http.StatusOK, gin.H{
			"code": 0,
			"data": req.Username,
		})
		return
	}

	c.JSON(http.StatusUnauthorized, gin.H{
		"code":    http.StatusUnauthorized,
		"message": "用户名或密码不正确",
	})
}
