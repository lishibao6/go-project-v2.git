package api

import (
	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps"
	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/apps/user"
	"gitee.com/lishibao6/go-project-v2/projects/vblog/api/conf"
	"github.com/gin-gonic/gin"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

func NewHTTPAPI() *HTTPAPI {
	return &HTTPAPI{}
}

// HTTPAPI定义用来 对外暴露HTTP服务，注册给协议Server(HTTP Server,Gin)
type HTTPAPI struct {
	log  logger.Logger
	conf *conf.Config
}

func (h *HTTPAPI) Init() error {
	h.log = zap.L().Named("api.user")
	h.conf = conf.GetConfig()
	return nil
}

func (h *HTTPAPI) Name() string {
	return user.AppName
}

// URI注册给Gin
func (h *HTTPAPI) Registry(r gin.IRouter) {
	r.POST("/auth", h.Auth)
}

func init() {
	apps.RegistryHttp(NewHTTPAPI())
}
