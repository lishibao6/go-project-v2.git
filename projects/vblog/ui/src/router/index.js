import { createRouter, createWebHistory } from 'vue-router'
import FrontendLayout from "../layout/FrontendLayout.vue"
import BackendLayout from "../layout/BackendLayout.vue"
import { beforeEachHandler, afterEachHandler } from "./permission";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: "/frontend/blogs"
    },
    {
      path: "/login",
      name: "LoginPage",
      component: () => import("@/views/login/LoginPage.vue")
    },
    {
      path: "/frontend",
      name: "frontend",
      component: FrontendLayout,
      children: [
        {
          path: "blogs",
          name: "BlogView",
          component: () => import("@/views/frontend/BlogView.vue"),
        },
        {
          path: "blogs/:id",
          name: "BlogDetail",
          component: () => import("@/views/frontend/BlogDetail.vue")
        }
      ]
    },
    {
      path: "/backend",
      name: "backend",
      component: BackendLayout,
      children: [
        {
          path: "blogs",
          name: "BlogList",
          component: () => import("@/views/backend/BlogList.vue")
        },
        {
          path: "blog_create",
          name: "BlogCreate",
          component: () => import("@/views/backend/BlogCreate.vue"),
        },
        {
          path: "tags",
          name: "TagList",
          component: () => import("@/views/backend/TagList.vue")
        }
      ]
    },
    {
      path: "/errors/403",
      name: "PermissionDeny",
      component: () => import("@/views/errors/PermissionDeny.vue"),
    },
    {
      path: "/:pathMatch(.*)*",
      name: "NotFound",
      component: () => import("@/views/errors/NotFound.vue")
    },

  ]
})

// 补充导航守卫
router.beforeEach(beforeEachHandler)
router.afterEach(afterEachHandler)

export default router
