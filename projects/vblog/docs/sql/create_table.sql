/*
据说MySQL一开始没有utf8mb4这个字符集，
因为utf8只支持每个字符最多三个字节，
而真正的UTF-8是每个字符最多四个字节，
这就造成UTF-8编码下的一些字符无法保存到数据库中，
为了修复这个bug而出现了utf8mb4这种字符集。


MySQL中常用的排序规则(这里以utf8字符集为例)主要有：utf8_general_ci、utf8_general_cs、utf8_unicode_ci等。

这里需要注意下ci和cs的区别:

ci的完整英文是’Case Insensitive’, 即“大小写不敏感”，a和A会在字符判断中会被当做一样的;
cs的完整英文是‘Case Sensitive’，即“大小写敏感”，a 和 A 会有区分；
*/
create database if not exists vblog default character set utf8mb4 collate utf8mb4_unicode_ci;
use vblog;
CREATE TABLE `blog` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '博客Id',
  `title_img` text COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题的图片',
  `title_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题的名称',
  `sub_title` text COLLATE utf8mb4_general_ci NOT NULL COMMENT '子标题',
  `content` text COLLATE utf8mb4_general_ci NOT NULL COMMENT '文章的内容',
  `author` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '文章作者',
  `create_at` int NOT NULL COMMENT '创建时间, 时间戳',
  `update_at` int NOT NULL COMMENT '更新时间',
  `publish_at` int NOT NULL COMMENT '发布时间',
  `status` tinyint(1) NOT NULL COMMENT '0: 草稿, 1: 发布',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `tag` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '标签Id',
  `key` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签的名称',
  `value` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签的值',
  `blog_id` int NOT NULL COMMENT '关联的文章',
  `create_at` int NOT NULL COMMENT '创建时间',
  `color` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签的颜色',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_id` (`key`,`value`) USING BTREE COMMENT 'key和value构成tag的唯一标识'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;