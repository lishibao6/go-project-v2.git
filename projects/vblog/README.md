# 微博客

## 架构

采用前端分离架构:

+ api:  后端接口服务
  + Go开发的Restful接口, Web框架采用Gin
  + 数据存储: MySQL(SQL+ORM)
+ ui:  vue3的前端
  + 前端框架:vue3, vue-router
  + UI组件: arcoUI(头条2021-12开源的vue3UI组件库)

## 设计

省略了需求的收集

### 功能设计

管理员:

+ 文章上传接口: 标题, 标签, 内容(图片看情况，需不需要上传, OSS)

浏览者:

+ 文章的列表接口: 获取所有的上传的文章列表(标签和标题已经元数据), 支持分页, 支持按照Tag过滤
+ 文章的详情接口: 核心获取文章的内容, 前端展示出来(markdown展示工具)

### 用户角色

+ 管理员
+ 博客浏览人员

+ 认证接口: basic auth(用户密码通过配置提供), 在接口上 通过中间件的方式实现接口认证

### 原型

vscode 查看原型的插件: Draw.io Integration

### 后端开发

#### 准备好开发环境

+ go 1.18
+ mysql: mysql  Ver 8.0.27 for Linux on x86_64 (MySQL Community Server - GPL)
  + 采用Docker搭建(docker安装教程直接百度)
  + 数据库连接工具(navicat, 尽量比较新的版本)
+ vscode开发环境
  + go开发环境, Go插件安装, 没安装写一个.go文件, 按照提示进行安装
  + windows shell环境准备: 参考: [VS Code 终端设置为Git Bash](https://www.cnblogs.com/remixnameless/p/14826532.html?tdsourcetag=s_pcqq_aiomsg)

#### 数据库设计

2张表:

+ blog: 用于存储文章
  + id: 博客id
  + title_img: 文章标题图片(oss的链接)
  + title_name: 文章标题
  + sub_title: 子标题
  + content: 博客内容
  + sumary: 摘要(不需要设计成字段，从内容当中提取100个字符)
  + author: 文章的作者
  + create_at: 创建时间
  + update_at: 更新时间
  + publish_at: 发布时间
  + status: 草稿/发布

+ tag: 用于存储文章标签,整个key,value组成一个tag
  + blog_id: 关联的博客
  + key: 标签名称
  + value: 标签的value
  + create_at: 创建时间：用于排序
  + color：标签颜色

#### 接口设计

RestFul接口就有2种资源(Resource): 文章,标签

+ 文章管理
  + 创建文章(Create): 管理员
    + URI: http://localhost/vblog/api/v1/blogs
    + Method: POST
    + Body: Blog创建对象
      + title_img: 文章标题图片(oss的链接)
      + title_name: 文章标题
      + sub_title: 子标题
      + content: 博客内容
      + author: 文章作者
  + 删除文章(Delete): 管理员
    + URI: http://localhost/vblog/api/v1/blogs/:blog_id
    + Method: DELETE
  + 更新文章
    + URI: http://localhost/vblog/api/v1/blogs/:blog_id
    + Method: PUT(全量更新)/PATCH(部分更新)
    + Body: 修改数据
      + title_img: 文章标题图片(oss的链接)
      + title_name: 文章标题
      + sub_title: 子标题
      + content: 博客内容
      + author: 文章作者
  + 文章列表(List): 无认证接口(浏览者/管理员)
    + URI: http://localhost/vblog/api/v1/blogs
    + Method: GET
    + Params:
      + page_size: 分页的大小，默认20个
      + page_number: 页码，默认第一页
      + status: 草稿(管理员才能查)/发布(都能查询)
      + keywords: 关键字搜索
  + 文章详情(Get)： 无认证接口(浏览者/管理员)
    + URI: http://localhost/vblog/api/v1/blogs/:blog_id
    + Method: GET
  + 文章发布：修改文章状态为发布
    + URI: http://localhost/vblog/api/v1/blogs/:blog_id/status
    + Method: POST
    + Body: 状态名称

+ 标签管理
  + 添加标签(Create)：管理员
    + URI: http://localhost/vblog/api/v1/tags
    + Method: POST
    + Body: Tag创建对象
      + blog_id: 关联的博客
      + key: 标签名称
      + value: 标签的value
      + color：标签的颜色
  + 删除标签(Delete)： 管理员
    + URI: http://localhost/vblog/api/v1/tags/:tag_id
    + Method: DELETE

## 工程架构

常见的代码组织方式有类:
+ 功能式分层架构
+ 分区式业务架构

一个文件就能成为一个项目: main.go
```go
func main() {
    // 读取程序配置
    

    // 启动gin API Server
    gin.Serve()


}
```

### 功能式分层架构

MVC代码组织方式:

![](./docs/images/mvc.jpeg)

在MVC架构中，通常将应用程序分为三个层次，每个层次都有不同的职责和任务：

1. 模型层（Model Layer）：负责数据的读取、存储和处理等任务。它通常包含了应用程序的业务逻辑，以及与数据存储和数据访问相关的代码。模型层是MVC架构中最底层的部分，与具体的数据结构和数据存储方式密切相关，但不与用户界面直接交互。

2. 视图层（View Layer）：负责应用程序的用户界面的呈现。它通常包含了应用程序的所有视觉元素和用户交互元素，例如窗口、按钮、文本框等。视图层是MVC架构中最上层的部分，与具体的数据结构和业务逻辑无关，但需要与控制器层进行交互。

3. 控制器层（Controller Layer）：用于将模型层和视图层连接起来，并处理用户的交互。它负责接收用户输入、解析用户请求、更新模型层的数据、管理视图层的显示等任务。控制器层是MVC架构中的中间层，它需要与模型层和视图层进行交互，并将它们解耦，以便于扩展和维护。

总之，MVC架构中的模型层、视图层和控制器层分别负责不同的任务，并通过明确定义的接口进行交互，以实现应用程序的分层和松散耦合。这种分层结构使得应用程序更加易于维护、扩展和测试，同时也使得各个部分的职责更加清晰明确。


### 分区式业务架构

具体参考project_arch ddd项目

### 分区式业务架构实现