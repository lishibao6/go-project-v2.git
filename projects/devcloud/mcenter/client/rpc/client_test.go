package rpc_test

import (
	"context"
	"testing"

	"gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/token"
	"gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/client/rpc"
)

func TestToken(t *testing.T) {
	c, err := rpc.NewClient("127.0.0.1:18050")
	if err != nil {
		t.Fatal(err)
	}

	tk, err := c.Token().ValidateToken(context.Background(), token.NewValidateTokenRequest("A2OGh9ORR8BSjY9juj898W9Z"))
	if err != nil {
		t.Fatal(err)
	}

	t.Log(tk)
}
