package policy

import context "context"

const (
	AppName = "policy"
)

type Service interface {
	CreatePolicy(context.Context, *CreatePolicyRequest) (*Policy, error)
	DeletePolicy(context.Context, *DeletePolicyRequest) (*Policy, error)
	RPCServer
}
