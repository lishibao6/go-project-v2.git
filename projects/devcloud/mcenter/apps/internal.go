package apps

import (
	// 注册所有内部服务模块, 无须对外暴露的服务, 用于内部依赖
	_ "gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/token/impl"
	_ "gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/user/impl"
)
