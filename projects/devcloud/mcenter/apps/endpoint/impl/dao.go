package impl

import "gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/endpoint"

func newDescribeEndpointRequest(req *endpoint.DescribeEndpointRequest) (*describeEndpointRequest, error) {
	return &describeEndpointRequest{req}, nil
}

type describeEndpointRequest struct {
	*endpoint.DescribeEndpointRequest
}
