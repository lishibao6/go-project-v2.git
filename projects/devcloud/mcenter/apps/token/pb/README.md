# 生成pb.go文件

## rpc.proto
```Bash
cd token/
protoc -I=. --go_out=./ --go_opt=module="gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/token" pb/rpc.proto
```

## token.proto
```Bash
cd token/
protoc -I=. --go_out=./ --go_opt=module="gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/token" pb/token.proto
```