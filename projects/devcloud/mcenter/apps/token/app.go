package token

import (
	"context"
	"math/rand"
	"net/http"
	"strings"
	"time"
)

const (
	AppName = "token"
)

type Service interface {
	// 颁发Token
	IssueToken(context.Context, *IssueTokenRequest) (*Token, error)
	// 撤销Token
	RevokeToken(context.Context, *RevokeTokenRequest) (*Token, error)
	// 切换Token空间
	ChangeNamespace(context.Context, *ChangeNamespaceRequest) (*Token, error)
	// 查询Token
	QueryToken(context.Context, *QueryTokenRequest) (*TokenSet, error)
	// RPC
	RPCServer
}

func NewToken(req *IssueTokenRequest) *Token {
	tk := &Token{
		AccessToken:      MakeBearer(24),
		RefreshToken:     MakeBearer(32),
		IssueAt:          time.Now().UnixMilli(),
		AccessExpiredAt:  req.ExpiredAt,
		RefreshExpiredAt: req.ExpiredAt * 4,
		GrantType:        req.GrantType,
		Type:             req.Type,
		Description:      req.Description,
		Status:           NewStatus(),
		Location:         req.Location,
	}

	switch req.GrantType {
	case GRANT_TYPE_PRIVATE_TOKEN:
		tk.Platform = PLATFORM_API
	default:
		tk.Platform = PLATFORM_WEB
	}
	return tk
}

// 判断token是否过期
func (t *Token) IsAccessTokenExpired() bool {
	// now > expired
	now := time.Now().UnixMilli() // 毫秒时间戳
	// t.AccessExpiredAt 单位是秒*1000变成毫秒
	// t.IssueAt 是毫秒时间戳
	expired := t.IssueAt + t.AccessExpiredAt*1000
	return now > expired
}

// 生成一个随机字符串
// MakeBearer https://tools.ietf.org/html/rfc6750#section-2.1
// b64token    = 1*( ALPHA / DIGIT /"-" / "." / "_" / "~" / "+" / "/" ) *"="
func MakeBearer(lenth int) string {
	// 定义字符列表
	charlist := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

	// 定义一个字符串切片
	t := make([]string, lenth)

	// 使用时间戳和长度作为随机数种子，生成随机字符串
	rand.Seed(time.Now().UnixNano() + int64(lenth) + rand.Int63n(10000))
	for i := 0; i < lenth; i++ {
		rn := rand.Intn(len(charlist))
		w := charlist[rn : rn+1]
		t = append(t, w)
	}

	// 将随机字符串拼接成一个字符串，并返回结果
	token := strings.Join(t, "")
	return token
}

func NewStatus() *Status {
	return &Status{
		IsBlock: false,
	}
}

// NewIssueTokenRequest 创建Token请求
func NewIssueTokenRequest() *IssueTokenRequest {
	return &IssueTokenRequest{
		// 单位秒
		ExpiredAt: 60,
	}
}

// NewRevokeTokenRequest 撤销Token请求
func NewRevokeTokenRequest() *RevokeTokenRequest {
	return &RevokeTokenRequest{}
}

// Authorization xxxxxxx
func GetTokenFromHTTPHeader(r *http.Request) string {
	auth := r.Header.Get("Authorization")
	info := strings.Split(auth, " ")
	if len(info) > 1 {
		return info[1]
	}

	return ""
}

func NewChangeNamespaceRequest() *ChangeNamespaceRequest {
	return &ChangeNamespaceRequest{}
}

func NewValidateTokenRequest(ak string) *ValidateTokenRequest {
	return &ValidateTokenRequest{
		AccessToken: ak,
	}
}
