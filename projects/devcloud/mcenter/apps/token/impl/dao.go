package impl

import (
	"context"
	"fmt"

	"gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/token"
	"github.com/infraboard/mcube/exception"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func (s *service) save(ctx context.Context, tk *token.Token) error {
	_, err := s.col.InsertOne(ctx, tk)
	if err != nil {
		return exception.NewInternalServerError("inserted token(%s) document error,%s",
			tk.AccessToken, err)
	}
	return nil
}

func (s *service) get(ctx context.Context, id string) (*token.Token, error) {
	filter := bson.M{"_id": id}

	ins := token.NewToken(token.NewIssueTokenRequest())
	err := s.col.FindOne(ctx, filter).Decode(ins)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, exception.NewNotFound("token %s not found", id)
		}
		return nil, exception.NewInternalServerError("find token %s error,%s", id, err)
	}
	return ins, nil
}

func (s *service) delete(ctx context.Context, ins *token.Token) error {
	if ins == nil || ins.AccessToken == "" {
		return fmt.Errorf("access token is nil")
	}

	filter := bson.M{"_id": ins.AccessToken}
	result, err := s.col.DeleteOne(ctx, filter)
	if err != nil {
		return exception.NewInternalServerError("delete token(%s) error,%s", ins.AccessToken, err)
	}

	if result.DeletedCount == 0 {
		return exception.NewNotFound("token %s not found", ins.AccessToken)
	}
	return nil
}
