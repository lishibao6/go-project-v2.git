package impl

import (
	"gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/token"
	"gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/conf"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"

	// 加载所有的provider
	"gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/token/provider"
	_ "gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/token/provider/all"
)

var (
	// Service服务实例
	svc = &service{}
)

type service struct {
	col *mongo.Collection
	log logger.Logger
	token.UnimplementedRPCServer
}

func (s *service) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return nil
	}
	s.col = db.Collection(s.Name())

	s.log = zap.L().Named(s.Name())

	// 初始化provider
	return provider.Init()
}
func (s *service) Name() string {
	return token.AppName
}
func (s *service) Registry(server *grpc.Server) {
	token.RegisterRPCServer(server, svc)
}

func init() {
	app.RegistryGrpcApp(svc)
	app.RegistryInternalApp(svc)
}
