package api

import (
	"gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/token"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/http/response"
)

func (h *handler) IssueToken(r *restful.Request, w *restful.Response) {
	req := token.NewIssueTokenRequest()

	//使用r.ReadEntity方法从RESTful API请求中读取请求体，并将其解析为IssueTokenRequest对象
	err := r.ReadEntity(req)
	if err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}

	tk, err := h.service.IssueToken(r.Request.Context(), req)
	if err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}

	response.Success(w.ResponseWriter, tk)
}

// Refresh Token必须传递, 判断撤销者身份
func (h *handler) RevokeToken(r *restful.Request, w *restful.Response) {
	qs := r.Request.URL.Query()
	req := token.NewRevokeTokenRequest()
	// 从HTTP Header中获取Access token
	req.AccessToken = token.GetTokenFromHTTPHeader(r.Request)
	// 从Query string获取Refresh Token
	req.RefreshToken = qs.Get("refresh_token")

	ins, err := h.service.RevokeToken(r.Request.Context(), req)
	if err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}

	response.Success(w.ResponseWriter, ins)
}

func (u *handler) ChangeNamespace(r *restful.Request, w *restful.Response) {
	req := token.NewChangeNamespaceRequest()
	err := r.ReadEntity(req)
	if err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}

	set, err := h.service.ChangeNamespace(r.Request.Context(), req)
	if err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}
	response.Success(w.ResponseWriter, set)
}
