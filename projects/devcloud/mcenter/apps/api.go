package apps

import (
	// 注册所有HTTP服务模块, 暴露给框架HTTP服务器加载
	_ "gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/token/api"
	_ "gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/user/api"
)
