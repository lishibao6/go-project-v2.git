package impl

import (
	"context"

	"gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/user"
	"gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/conf"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"google.golang.org/grpc"
)

var (
	// service接口的实现类
	svr = &impl{}
)

type impl struct {
	col *mongo.Collection
	// 日志
	log logger.Logger
	// 作为rpc服务的一个实现类
	user.UnimplementedRPCServer
}

func (s *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return nil
	}
	// 表的名称user
	s.col = db.Collection(s.Name())

	// 设置唯一键
	indexs := []mongo.IndexModel{
		{
			Keys: bsonx.Doc{{Key: "create_at", Value: bsonx.Int32(-1)}},
		},
		{
			Keys: bsonx.Doc{
				{Key: "spec.domain", Value: bsonx.Int32(-1)},
				{Key: "spec.username", Value: bsonx.Int32(-1)},
			},
			Options: options.Index().SetUnique(true),
		},
	}

	_, err = s.col.Indexes().CreateMany(context.Background(), indexs)
	if err != nil {
		return err
	}

	s.log = zap.L().Named(s.Name())
	return nil
}

func (s *impl) Name() string {
	return user.AppName
}

// 把该实例类作为grpc对外提供服务
func (s *impl) Registry(server *grpc.Server) {
	user.RegisterRPCServer(server, svr)
}

func init() {
	app.RegistryGrpcApp(svr)
	app.RegistryInternalApp(svr)
}
