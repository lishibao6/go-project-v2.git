package api

import (
	"fmt"

	"gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/user"
	restfulspec "github.com/emicklei/go-restful-openapi"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/http/label"
	"github.com/infraboard/mcube/http/response"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

var (
	primaryHandler = &primary{}
)

// 子账号管理
type primary struct {
	service user.Service
	log     logger.Logger
}

func (h *primary) Name() string {
	return "user/primary"
}

func (h *primary) Version() string {
	return "v1"
}

func (h *primary) Config() error {
	h.log = zap.L().Named(user.AppName)
	h.service = app.GetGrpcApp(user.AppName).(user.Service)
	return nil
}

// 需要提供的Restful接口
// 哪些接口 适合哪些类型的用户使用
// 只能主账号调用
func (h *primary) Registry(ws *restful.WebService) {
	tags := []string{"子账号管理"}

	ws.Route(ws.POST("").To(h.CreateUser).
		Metadata(label.Resource, "子账号").
		Metadata(label.Auth, true).
		Metadata(label.Allow, user.TYPE_PRIMARY).
		// 下面的都是文档说明，在swagger中会用到
		Doc("创建子账号").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Reads(user.CreateUserRequest{}).
		Writes(user.User{}))

	ws.Route(ws.GET("/").To(h.QueryUser).
		Metadata(label.Resource, "子账号").
		Metadata(label.Auth, true).
		Metadata(label.Allow, user.TYPE_PRIMARY).
		Doc("查询子账号列表").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Reads(user.CreateUserRequest{}).
		Writes(response.NewData(user.User{})))

	ws.Route(ws.GET("/{id}").To(h.DescribeUser).
		Metadata(label.Resource, "子账号").
		Metadata(label.Auth, true).
		Doc("查询子用户明细").
		Param(ws.PathParameter("id", "identifier of the user").DataType("string")).Metadata(restfulspec.KeyOpenAPITags, tags).
		Writes(response.NewData(user.User{})).
		Returns(200, "OK", response.NewData(user.User{})).
		Returns(404, "Not Found", nil))
}

// 创建子账号(主账号)
func (p *primary) CreateUser(r *restful.Request, w *restful.Response) {
	// 通过HTTP获取用户请求数据
	req := user.NewCreateUserRequest()

	err := r.ReadEntity(req)
	if err != nil {
		response.Failed(w.ResponseWriter, err)
	}

	req.Type = user.TYPE_SUB
	req.CreateBy = user.CREATE_BY_ADMIN

	user, err := p.service.CreateUser(r.Request.Context(), req)
	if err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}

	response.Success(w.ResponseWriter, user)
}

// 查询子账号
func (p *primary) QueryUser(r *restful.Request, w *restful.Response) {
	req := user.NewQueryUserRequestFromHTTP(r.Request)

	// 控制参数范围
	req.WithType(user.TYPE_SUB)
	ins, err := p.service.QueryUser(r.Request.Context(), req)
	if err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}

	response.Success(w.ResponseWriter, ins)
}

func (p *primary) DescribeUser(r *restful.Request, w *restful.Response) {
	// url path 获取变量id  {id}
	req := user.NewDescriptUserRequestWithId(r.PathParameter("id"))
	ins, err := p.service.DescribeUser(r.Request.Context(), req)
	if err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}

	if !ins.Spec.Type.Equal(user.TYPE_SUB) {
		response.Failed(w.ResponseWriter, fmt.Errorf("not an sub account"))
		return
	}

	// restful返回的数据，需要脱敏的，但是rpc访问的数据 不需要脱敏
	ins.Desensitize()

	response.Success(w.ResponseWriter, ins)
}
