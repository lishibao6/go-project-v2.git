package cmd

import (
	"context"
	"fmt"

	"gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/domain"
	"gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/user"
	"github.com/infraboard/mcube/app"
	"github.com/spf13/cobra"
)

var (
	createTableFilePath string
)

// initCmd represents the start command
var initCmd = &cobra.Command{
	Use:   "init",
	Short: "mcenter 服务初始化",
	Long:  "mcenter 服务初始化",
	RunE: func(cmd *cobra.Command, args []string) error {
		// 初始化全局变量
		if err := loadGlobalConfig(confType); err != nil {
			return err
		}

		// 初始化全局日志配置
		err := loadGlobalLogger()
		if err != nil {
			return err
		}

		// 初始化全局app
		err = app.InitAllApp()
		if err != nil {
			return err
		}

		// 创建一个主账号
		us := app.GetInternalApp(user.AppName).(user.Service)
		createUserReq := user.NewCreateUserRequest()
		createUserReq.CreateBy = user.CREATE_BY_ADMIN
		createUserReq.Description = "超级管理员"
		createUserReq.Domain = domain.DEFAULT_DOMAIN
		createUserReq.Type = user.TYPE_SUPPER
		createUserReq.Username = "admin"
		createUserReq.Password = "123456"
		u, err := us.CreateUser(context.Background(), createUserReq)
		if err != nil {
			return err
		}

		fmt.Printf("初始化管理员用户: %s 成功\n", u.Spec.Username)
		return nil
	},
}

func init() {
	RootCmd.AddCommand(initCmd)
}
