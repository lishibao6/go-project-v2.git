package auth

import (
	"fmt"

	"gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/token"
	"gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter/apps/user"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/http/label"
	"github.com/infraboard/mcube/http/response"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

// FilterFunction definitions must call ProcessFilter on the FilterChain to pass on the control and eventually call the RouteFunction
// type FilterFunction func(*Request, *Response, *FilterChain)

func NewAuther() *Auther {
	return &Auther{
		log: zap.L().Named("auther.http"),
		tk:  app.GetInternalApp(token.AppName).(token.Service),
	}
}

type Auther struct {
	log logger.Logger
	tk  token.Service
}

func (a *Auther) GoRestfulAuthFunc(req *restful.Request, resp *restful.Response, next *restful.FilterChain) {
	// 请求拦截
	meta := req.SelectedRoute().Metadata()
	a.log.Debugf("router meta: %s", meta)

	isAuth, ok := meta[label.Auth]
	// 有认证标签,并且开启了认证
	if ok && isAuth.(bool) {
		// 获取token
		ak := token.GetTokenFromHTTPHeader(req.Request)

		tk, err := a.tk.ValidateToken(req.Request.Context(), token.NewValidateTokenRequest(ak))
		if err != nil {
			response.Failed(resp.ResponseWriter, err)
			return
		}

		// 判断用户权限
		v, ok := meta[label.Allow]
		if ok {
			ut := v.(user.TYPE)
			// 权限编号来判断
			if tk.UserType < ut {
				response.Failed(resp.ResponseWriter, fmt.Errorf("permission deny: %s, required:%s", tk.UserType, ut))
				return
			}
		}

		// next flow
		next.ProcessFilter(req, resp)

		// 响应拦截
		a.log.Debugf("%s", resp)
	}
}
