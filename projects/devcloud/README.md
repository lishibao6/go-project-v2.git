# 研发云


## 功能
+ 用户管理: 管理登录用户
    + 用户的CRUD
+ 权限管理：管理哪些用户可以使用哪些功能
    + 策略管理(PRBAC): 基于角色的策略管理模型
        + 角色: 一组功能的集合[那个功能(cmdb create host)]
            + 功能列表: 整体个平台有哪些功能(平台所有服务Restful的接口), service:resource:action (功能条目)
                + 功能条目注册: 服务把自己的接口(resource:action),注册到权限给权限管理模块
        + 用户: 用户是什么角色
        + 空间: 如何来进行资源的隔离(各业务线才能操作各自的服务)
        + 环境: 开发环境/测试环境/生产环境

+ 服务管理: 对开发中的服务进行关联
    + 服务创建
        + 系统服务
            + 平台需要运行的基础服务
        + 业务服务
            + 开发具体的商业应用的服务
    + 服务查看

+ 资产管理: 对业务开发需要的资源进行关联
    + 主机
    + Rds
    + Bucket
    + Redis
    + Domain
+ 发布平台: 基于K8s的一套微服务部署平台(Deploy/DaemonSet/...)
    + 服务的发布
    + 服务的查询

## 原型



## 架构


## 微服务开发

+ 用户中心(mcenter)
+ 审计中心(maudit)
+ 资源中心(cmdb)

+ 微服务网关(traefik)

## 脚手架生成mcenter功能代码
```Bash
devcloud $ mcube.exe project init 
? 请输入项目包名称: (gitee.com/go-course/mcube-demo) gitee.com/lishibao6/go-pr? 请输入项目包名称: gitee.com/lishibao6/go-project-v2/projects/devcloud/mcenter
? 请输入项目描述: 用户中心

? 请输入项目描述: 用户中心
? 是否接入权限中心[keyauth] No
? 选择数据库类型: MongoDB
? MongoDB服务地址,多个地址使用逗号分隔: (127.0.0.1:27017)

? MongoDB服务地址,多个地址使用逗号分隔: 127.0.0.1:27017
? 认证数据库名称: mcenter
? 认证用户: mcenter
? 生成样例代码 Yes   
? 选择HTTP框架: go-restful
项目初始化完成, 项目结构如下: 
├───.gitignore (296b) 
├───.mcube.yaml (233b)
├───.vscode
│       └───settings.json (233b)
├───README.md (4227b)
├───apps
│       ├───all
│       │       ├───api.go (171b)
│       │       ├───impl.go (202b)
│       │       └───internal.go (107b)
│       └───book
│               ├───api
│               │       ├───book.go (2283b)
│               │       └───http.go (2273b)
│               ├───app.go (2213b)
│               ├───impl
│               │       ├───book.go (1697b)
│               │       ├───dao.go (3094b)
│               │       └───impl.go (881b)
│               └───pb
│                       └───book.proto (2383b)
├───client
│       ├───client.go (1010b)
│       ├───client_test.go (694b)
│       └───config.go (164b)
├───cmd
│       ├───init.go (471b)
│       ├───root.go (1290b)
│       └───start.go (3858b)
├───conf
│       ├───config.go (3654b)
│       ├───load.go (720b)
│       └───log.go (365b)
├───docs
│       ├───README.md (15b)
│       └───schema
│               └───tables.sql (849b)
├───etc
│       ├───config.env (479b)
│       ├───config.toml (320b)
│       └───unit_test.env (17b)
├───go.mod (66b)
├───main.go (123b)
├───makefile (3014b)
├───protocol
│       ├───grpc.go (1367b)
│       └───http.go (2973b)
├───swagger
│       └───docs.go (744b)
└───version
        └───version.go (631b
```

下载依赖
```
go mod tidy
```

## Copy依赖的Protobuf文件

```
# 使用 pb指令来完成protobuf文件copy, 但是windows由于路径文件不行
$ make pb
mkdir: created directory 'common'
mkdir: created directory 'common/pb'
mkdir: created directory 'common/pb/github.com'
mkdir: created directory 'common/pb/github.com/infraboard'
mkdir: created directory 'common/pb/github.com/infraboard/mcube'
mkdir: created directory 'common/pb/github.com/infraboard/mcube/pb'
cp: cannot stat 'E:Golang/pkg/mod/github.com/infraboard/mcube@v1.9.0/pb/*': No such file or directory
make: *** [makefile:64: pb] Error 1
```

需要对protobuf做版本管理: mcube这个库没有石油版本 git tag, 比如当前mcube 1.9, copy 1.9版本的 protobuf文件， 

怎么获取对应版本的protobuf文件
```sh
# 获取当前项目依赖cmube的版本
$ go get
$ go list -m  "github.com/infraboard/mcube" | cut -d' ' -f2
v1.9.15

# 找到该版本的源码文件, 源码文件里面有protobuf文件, 也就是对应1.9版本的protobuf文件
$ ls $GOPATH/pkg/mod/github.com/infraboard/ | grep mcube
mcube@v1.4.3
mcube@v1.9.0
mcube@v1.9.15
# 找到了对应版本的protobuf文件
$ ls $GOPATH/pkg/mod/github.com/infraboard/mcube\@v1.9.15/pb/
example  http  page  request  resource  response

# 把这些文件copy到当前项目来
$ cp -r $GOPATH/pkg/mod/github.com/infraboard/mcube\@v1.9.15/pb/* common/pb/github.com/infraboard/mcube/pb

# 清除.go的文件，只要protobuf源文件
rm -rf common/pb/github.com/infraboard/mcube/pb/*/*.go

# 查看protobu是否copy过来
$ ls common/pb/github.com/infraboard/mcube/pb/
example  http  page  request  resource  response
```


## 编译 protobuf


make install: Install depence go package
```
# protoc-gen-go
$go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
# protoc-gen-go-grpc
$go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
# protoc-go-inject-tag 注入tag的插件
# 如果你要为你的生成后的结构体补充自定义标签: protoc-go-inject-tag
# 在字段上方加入注解: // @gotags: json:"create_at" bson:"create_at"
# protoc-go-inject-tag扫描注解，修改生成的struct 的tag
$go install github.com/favadi/protoc-go-inject-tag@latest
```

?, 工程里面生成一个样例代码, 这样样例代码是有protobuf文件, 为了样例跑起来，需要编译protobuf文件

make gen
```
    # 编译protobuf文件, apps/<模块名称>/pb/*.proto
	@protoc -I=. -I=common/pb --go_out=. --go_opt=module=${PKG} --go-grpc_out=. --go-grpc_opt=module=${PKG} apps/*/pb/*.proto
	@go fmt ./...

    # protobuf的tag注入库: protoc-go-inject-tag, 通过执行下面的插件来二次处理protobuf, 完成标签的重写
	@protoc-go-inject-tag -input=apps/*/*.pb.go
    # 为emnu类型 添加string描述
	@mcube generate enum -p -m apps/*/*.pb.go
```

## 安装数据库

```
docker pull mongo
docker run --name mongo --restart=always -itd -p 27017:27017 mongo
```

进入mongo的本地shell 添加用户: demo用户
```sh
$ docker exec -it mongo  mongo
> use demo
# 命令输入结果如下
switched to db mcenter
> db.createUser({user: "demo", pwd: "123456", roles: [{ role: "dbOwner", db: "demo" }]})
# 输入结果如下
Successfully added user: {
	"user" : "mcenter",
	"roles" : [
		{
			"role" : "dbOwner",
			"db" : "mcenter"
		}
	]
}
```

确认配置文件里面的mongo配置: etc/config.toml
```toml
[mongodb]
endpoints = ["127.0.0.1:27017"]
username = "mcenter"
password = "123456"
database = "mcenter"
```


## 启动服务

```sh
$ make run
2022-10-01T17:33:51.182+0800    INFO    [INIT]  cmd/start.go:154        log level: debug
2022-10-01T17:33:51.215+0800    INFO    [CLI]   cmd/start.go:92 loaded grpc app: [book]
2022-10-01T17:33:51.216+0800    INFO    [CLI]   cmd/start.go:93 loaded http app: [book]
2022-10-01T17:33:51.216+0800    INFO    [CLI]   cmd/start.go:95 loaded internal app: []
2022-10-01T17:33:51.219+0800    INFO    [GRPC Service]  protocol/grpc.go:54     GRPC 服务监听地址: 127.0.0.1:18050
2022-10-01T17:33:51.219+0800    INFO    [HTTP Service]  protocol/http.go:78     Get the API using http://127.0.0.1:8050/apidocs.json
2022-10-01T17:33:51.220+0800    INFO    [HTTP Service]  protocol/http.go:81     HTTP服务启动成功, 监听地址: 127.0.0.1:8050
```

## 查看API文档

https://petstore.swagger.io/  输入： http://127.0.0.1:8050/apidocs.json