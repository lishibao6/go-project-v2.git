package pb_test

import (
	"fmt"
	"testing"

	"gitee.com/lishibao6/go-project-v2/micro/rpc/protobuf/serialization/pb"
	"google.golang.org/protobuf/proto"
)

func TestMarshal(t *testing.T) {
	b := &pb.Blog{
		Title:       "GO 语言Protobuf讲解",
		Summary:     "GO 语言Protobuf讲解",
		Content:     "xxx",
		Author:      "zhangsan",
		IsPublished: true,
	}

	// 序列化(编码)
	encodedB, err := proto.Marshal(b)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println("encodedB: ", encodedB)
	// encodedB:  [10 23 71 79 32 232 175 173 232 168 128 80 114 111 116 111 98 117 102 232 174 178 232 167 163 18 23 71 79 32 232 175 173 232 168 128 80 114 111 116 111 98 117 102 232 174 178 232 167 163 26 3 120 120 120 34 8 122 104 97 110 103 115 97 110 40 1]

	// 反序列化(解码)
	b2 := &pb.Blog{}
	err = proto.Unmarshal(encodedB, b2)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println("b2: ", b2)
	// b2:  title:"GO 语言Protobuf讲解"  summary:"GO 语言Protobuf讲解"  content:"xxx"  author:"zhangsan"  is_published:true
}
