package pb_test

import (
	"fmt"
	"log"
	"testing"

	"gitee.com/lishibao6/go-project-v2/micro/rpc/protobuf/sample/other_pb"
	"gitee.com/lishibao6/go-project-v2/micro/rpc/protobuf/sample/pb"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/anypb"
)

type Blog struct {
	Status pb.STATUS
}

func (b *Blog) CheckStatus() string {
	switch b.Status {
	case pb.STATUS_DRAFT:
		return "文章状态是草稿"
	case pb.STATUS_PUBLISHED:
		return "文章状态是发布"
	default:
		return "文章状态非法"
	}
}

func TestEnum1(t *testing.T) {
	b1 := &Blog{Status: pb.STATUS_DRAFT}
	t.Log("b1:", b1.CheckStatus())

	b2 := &Blog{Status: pb.STATUS_PUBLISHED}
	t.Log("b2:", b2.CheckStatus())

	b3 := &Blog{Status: 3}
	t.Log("b3:", b3.CheckStatus())
}

func TestEnumAlias(t *testing.T) {
	m1 := &pb.MyMessage1{
		Status: pb.MyMessage1_STARTED,
	}
	m2 := &pb.MyMessage1{
		Status: pb.MyMessage1_RUNNING,
	}
	t.Log(m1, m2)
}

func TestEnumReserved(t *testing.T) {
	f1 := pb.Foo_UNKNOWN
	f2 := pb.Foo_RUNNING
	f3 := pb.Foo_STARTED
	fmt.Println(f1, f2, f3)
}

func TestArray(t *testing.T) {
	blog1 := &pb.Blog{
		Title:  "test1",
		Author: "zhangsan",
		Tags:   map[string]string{"color": "red"},
	}
	blog2 := &pb.Blog{
		Title:  "test2",
		Author: "lisi",
		Tags:   map[string]string{"color": "blue"},
	}

	blogSet := &pb.BlogSet{
		Total: proto.Int64(2),
		Items: []*pb.Blog{blog1, blog2},
	}

	// 将博客列表编码为二进制数据
	data, err := proto.Marshal(blogSet)
	if err != nil {
		log.Fatal("marshaling error: ", err)
	}

	// 将二进制数据解码为博客列表
	var decodedBlogSet pb.BlogSet
	err = proto.Unmarshal(data, &decodedBlogSet)
	if err != nil {
		log.Fatal("unmarshaling error: ", err)
	}

	// 打印解码后的博客列表
	t.Log(decodedBlogSet.String())
}

func TestOneOf(t *testing.T) {
	// 创建一个资源状态变更事件
	changeEvent := &pb.Event{
		Type: pb.TYPE_RESOURCE_CHANGE,
		Data: &pb.Event_ResourceChange{
			ResourceChange: &pb.ResourceChange{
				ResourceId: "ecs-001",
				Action:     "create",
			},
		},
	}

	// 将事件对象编码为二进制数据
	data, err := proto.Marshal(changeEvent)
	if err != nil {
		fmt.Println("marshaling error: ", err)
		return
	}

	// 将二进制数据解码为事件对象
	var decodedEvent pb.Event
	err = proto.Unmarshal(data, &decodedEvent)
	if err != nil {
		fmt.Println("unmarshaling error: ", err)
		return
	}

	// 打印解码后的事件对象
	t.Log(decodedEvent.String())

	alertEvent := &pb.Event{
		Type: pb.TYPE_RESOURCE_ALTERT,
		Data: &pb.Event_ResourceAlert{
			ResourceAlert: &pb.ResourceAlert{
				ResouceId: "ecs-002",
				Level:     "warning",
			},
		},
	}

	// 将事件对象编码为二进制数据
	data, err = proto.Marshal(alertEvent)
	if err != nil {
		fmt.Println("marshaling error: ", err)
		return
	}

	// 将二进制数据解码为事件对象
	err = proto.Unmarshal(data, &decodedEvent)
	if err != nil {
		fmt.Println("unmarshaling error: ", err)
		return
	}

	// 打印解码后的事件对象
	t.Log(decodedEvent.String())
}

func TestAny(t *testing.T) {
	// any的传参
	any, err := anypb.New(&pb.Blog{
		Title:  "test",
		Author: "zhangsan",
	})
	if err != nil {
		panic(err)
	}
	device := pb.Device{
		DeviceType: "a1",
		Data:       any,
	}
	fmt.Println("device: ", device.String())
	//device:  device_type:"a1"  data:{[type.googleapis.com/hello.Blog]:{title:"test"  author:"zhangsan"}}

	// 如何解析any
	blogA1 := &pb.Blog{}
	err = device.Data.UnmarshalTo(blogA1)
	if err != nil {
		panic(err)
	}
	fmt.Println("blogA1: ", blogA1) //blogA1:  title:"test"  author:"zhangsan"
}

func TestNest(t *testing.T) {
	// 创建一个 InnerMessage 实例
	innerMessage := &pb.InnerMessage{
		InnerField1: "hello",
		InnerField2: 123,
	}

	// 创建一个 OuterMessage 实例
	outerMessage := &pb.OuterMessage{
		OuterField1:  "world",
		InnerMessage: innerMessage,
	}

	// 将消息实例编码为二进制数据
	data, err := proto.Marshal(outerMessage)
	if err != nil {
		fmt.Println("marshaling error: ", err)
		return
	}

	// 将二进制数据解码为消息实例
	var decodedMessage pb.OuterMessage
	err = proto.Unmarshal(data, &decodedMessage)
	if err != nil {
		fmt.Println("unmarshaling error: ", err)
		return
	}

	// 打印解码后的消息实例
	fmt.Println(decodedMessage.String())
}

func TestOtherPb(t *testing.T) {
	// 创建一个 BlogSet 实例
	blogSet := &pb.BlogSet{
		Items: []*pb.Blog{
			{Title: "blog1", Author: "lisi"},
			{Title: "blog2", Author: "lisi"},
			{Title: "blog3", Author: "lisi"},
		},
	}

	// 创建一个 BlogSetPage 实例
	blogSetPage := &other_pb.BlogSetPage{
		Set: blogSet,
	}
	fmt.Println(blogSetPage.String())
}
