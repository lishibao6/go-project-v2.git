# 编译文件

需要在sample这个目录下执行
## 枚举
```sh
protoc -I=. --go_out=./pb --go_opt=module="gitee.com/lishibao6/go-project-v2/micro/rpc/protobuf/sample/pb" pb/enum.proto
```

## 数组
```sh
protoc -I=. --go_out=./pb --go_opt=module="gitee.com/lishibao6/go-project-v2/micro/rpc/protobuf/sample/pb" pb/array.proto
```

## oneof
```sh
protoc -I=. --go_out=./pb --go_opt=module="gitee.com/lishibao6/go-project-v2/micro/rpc/protobuf/sample/pb" pb/oneof.proto
```

## any
```sh
protoc -I=. -I=/usr/local/include  --go_out=./pb --go_opt=module="gitee.com/lishibao6/go-project-v2/micro/rpc/protobuf/sample/pb" pb/any.proto
```

## 类型嵌套
```sh
protoc -I=. --go_out=./pb --go_opt=module="gitee.com/lishibao6/go-project-v2/micro/rpc/protobuf/sample/pb" pb/nest.proto
```

## 引用包
```sh
protoc -I=. -I=/usr/local/include  --go_out=./other_pb --go_opt=module="gitee.com/lishibao6/go-project-v2/micro/rpc/protobuf/sample/other_pb" other_pb/other.proto
```