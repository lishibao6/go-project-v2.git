module gitee.com/lishibao6/go-project-v2/micro/rpc/rpc_protobuf

go 1.20

require (
	github.com/golang/snappy v0.0.4
	google.golang.org/protobuf v1.30.0
)
