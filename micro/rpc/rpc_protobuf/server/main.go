package main

import (
	"fmt"
	"log"
	"net"
	"net/rpc"

	"gitee.com/lishibao6/go-project-v2/micro/rpc/rpc_protobuf/codec/server"
	"gitee.com/lishibao6/go-project-v2/micro/rpc/rpc_protobuf/service"
)

var _ service.HelloService = (*HelloService)(nil)

type HelloService struct{}

func (s *HelloService) Greet(request *service.Request, resp *service.Response) error {
	resp.Value = fmt.Sprintf("hello, %s", request.Value)
	return nil
}

func main() {
	rpc.RegisterName("HelloService", &HelloService{})

	listener, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Fatal("ListenTCP error:", err)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal("Accept error:", err)
			continue
		}

		// 自己写的基于Protobuf 的 ServerCodec
		svc := server.NewServerCodec(conn)
		go rpc.ServeCodec(svc)
	}
}

// go run server/main.go
