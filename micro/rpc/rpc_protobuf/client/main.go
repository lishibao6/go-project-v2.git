package main

import (
	"fmt"
	"net"
	"net/rpc"

	"gitee.com/lishibao6/go-project-v2/micro/rpc/rpc_protobuf/codec/client"
	"gitee.com/lishibao6/go-project-v2/micro/rpc/rpc_protobuf/service"
)

func NewHelloServiceClient(network, address string) *HelloServiceClient {
	// 通过网络来和rpc server建立通信
	conn, err := net.Dial(network, address)
	if err != nil {
		panic(err)
	}

	// 使用自己编写的Protobuf Codec
	clientCodec := client.NewClientCodec(conn)
	client := rpc.NewClientWithCodec(clientCodec)

	return &HelloServiceClient{
		client: client,
	}
}

type HelloServiceClient struct {
	client *rpc.Client
}

// RPC Client
func (c *HelloServiceClient) Greet(req *service.Request, resp *service.Response) error {
	return c.client.Call("HelloService.Greet", req, resp)
}

func main() {
	client := NewHelloServiceClient("tcp", "localhost:1234")
	req := service.Request{Value: "alice"}
	resp := service.Response{}
	err := client.Greet(&req, &resp)
	if err != nil {
		panic(err)
	}
	fmt.Println(resp.Value)
}

// $ go run client/main.go
// hello, alice
