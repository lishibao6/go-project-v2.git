package main

import (
	"fmt"
	"log"
	"net"
	"net/rpc"

	"gitee.com/lishibao6/go-project-v2/micro/rpc/rpc_interface/service"
)

// 如何约束HelloService实现 HelloService接口
// var a1 service.HelloService = &HelloService{}
// 多个一个变量a1 --> _
// 多个一个值 &HelloService{} 多个一个开销, nil

// a1.(service.HelloService)
// (*HelloService)(nil) 表示的是一个*HelloService的nil   (int32)(10)  (*int32)(nil)
// 下面的代码用来判断HelloService结构体是否实现了service.HelloService接口
var _ service.HelloService = (*HelloService)(nil)

// 上面代码的等价形式
// var client *HelloServiceClient = nil // 定义一个空指针
// var _ HelloService = client         // 将空指针转换为 HelloService 接口类型，并将其赋值给 _ 变量

type HelloService struct{}

func (s *HelloService) Greet(request string, resp *string) error {
	*resp = fmt.Sprintf("hello, %s", request)
	return nil
}

func main() {
	rpc.RegisterName("HelloService", &HelloService{})
	listener, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Fatal("ListenTCP error", err)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal("Accept error:", err)
			continue
		}
		go rpc.ServeConn(conn)
	}
}

// cd rpc_interface
// go run server/main.go
