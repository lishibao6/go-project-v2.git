package main

import (
	"fmt"
	"net"
	"net/rpc"

	"gitee.com/lishibao6/go-project-v2/micro/rpc/rpc_interface/service"
)

var _ service.HelloService = (*HelloServiceClient)(nil)

func NewHelloServiceClient(network, address string) *HelloServiceClient {
	// 通过网络来和rpc server建立通信
	conn, err := net.Dial(network, address)
	if err != nil {
		panic(err)
	}

	client := rpc.NewClient(conn)
	return &HelloServiceClient{
		client: client,
	}
}

type HelloServiceClient struct {
	client *rpc.Client
}

// RPC Client
func (c *HelloServiceClient) Greet(request string, resp *string) error {
	return c.client.Call("HelloService.Greet", request, resp)
}

// 作为RPC client的使用方
func main() {
	client := NewHelloServiceClient("tcp", "localhost:1234")
	var resp string
	err := client.Greet("alice", &resp)
	if err != nil {
		panic(err)
	}
	fmt.Println(resp)
}

// cd rpc_interface
// go run client/main.go
// hello, alice
