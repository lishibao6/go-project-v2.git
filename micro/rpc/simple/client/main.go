package main

import (
	"fmt"
	"net"
	"net/rpc"
)

func main() {
	// 通过网络来和rpc server建立通信
	conn, err := net.Dial("tcp", "localhost:1234")
	if err != nil {
		panic(err)
	}

	// 把network交给 rpc框架处理
	client := rpc.NewClient(conn)

	// 通过client来调用server方法
	// 客户端类似于这样来调用  HelloService.Greet()
	// Greet(request string, resp *string)
	var resp string
	if err := client.Call("HelloService.Greet", "alice", &resp); err != nil {
		panic(err)
	}

	// 处理返回结果
	fmt.Println(resp)
}

// go run micro/rpc/simple/client/main.go
// hello, alice
