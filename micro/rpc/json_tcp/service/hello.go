package service

type HelloService interface {
	Greet(string, *string) error
}
