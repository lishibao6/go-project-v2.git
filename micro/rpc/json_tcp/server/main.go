package main

import (
	"fmt"
	"log"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"

	"gitee.com/lishibao6/go-project-v2/micro/rpc/json_tcp/service"
)

var _ service.HelloService = (*HelloService)(nil)

type HelloService struct{}

func (s *HelloService) Greet(req string, resp *string) error {
	*resp = fmt.Sprintf("Hello, %s", req)
	return nil
}

func main() {
	rpc.RegisterName("HelloService", &HelloService{})
	listener, err := net.Listen("tcp", "localhost:1234")
	if err != nil {
		log.Fatal("ListenerTCP error:", err)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal("Accept error:", err)
			continue
		}
		// rpc框架为提供了基于Json的 服务端的编解码器: jsonrpc.ServerCodec
		svc := jsonrpc.NewServerCodec(conn)
		go rpc.ServeCodec(svc)

		// go rpc.ServeConn(conn)
	}
}

// go run server/main.go
