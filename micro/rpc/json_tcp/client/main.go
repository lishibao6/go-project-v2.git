package main

import (
	"fmt"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"

	"gitee.com/lishibao6/go-project-v2/micro/rpc/json_tcp/service"
)

var _ service.HelloService = (*HelloServiceClient)(nil)

func NewHelloServiceClient(network, address string) *HelloServiceClient {
	// 通过网络来和rpc server建立通信
	conn, err := net.Dial(network, address)
	if err != nil {
		panic(err)
	}

	// 把network交给 rpc框架处理, 默认使用Gob
	// client := rpc.NewClient(conn)

	// 客户端使用Json编解码器
	clientCodec := jsonrpc.NewClientCodec(conn)
	client := rpc.NewClientWithCodec(clientCodec)
	return &HelloServiceClient{
		client: client,
	}
}

type HelloServiceClient struct {
	client *rpc.Client
}

func (c *HelloServiceClient) Greet(req string, resp *string) error {
	return c.client.Call("HelloService.Greet", req, resp)
}

func main() {
	client := NewHelloServiceClient("tcp", "localhost:1234")
	var resp string
	err := client.Greet("alice", &resp)
	if err != nil {
		panic(err)
	}
	fmt.Println(resp)
}

// 方法一：go run client/main.go
// 方法二： 安装nc :https://eternallybored.org/misc/netcat/
// echo -e '{"method":"HelloService.Greet","params":["alice"],"id":1}' | nc localhost 1234
// {"id":1,"result":"Hello, alice","error":null}
