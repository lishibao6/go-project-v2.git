package gob_test

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"testing"
)

func GobEncode(val any) ([]byte, error) {
	buf := bytes.NewBuffer([]byte{})
	encoder := gob.NewEncoder(buf)
	if err := encoder.Encode(val); err != nil {
		return []byte{}, err
	}
	return buf.Bytes(), nil
}

func GobDecode(data []byte, value any) error {
	reader := bytes.NewReader(data)
	decoder := gob.NewDecoder(reader)
	return decoder.Decode(value)
}

type Blog struct {
	Title       string
	Author      string
	Summary     string
	IsPublished bool
	Content     string
}

func TestGob(t *testing.T) {
	b := &Blog{
		Title:       "test",
		Author:      "zhangsan",
		Summary:     "J",
		Content:     "abc",
		IsPublished: true,
	}

	// Gob编码
	encodeB, err := GobEncode(b)
	if err != nil {
		t.Fatal(err)
	}

	// Json编码
	jsonB, err := json.Marshal(b)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("encodeB:", encodeB)
	t.Log("len(encodeB):", len(encodeB)) //110

	t.Log("jsonB:", jsonB)
	t.Log("len(jsonB):", len(jsonB)) //85

	// Gob解码
	decodeB := Blog{}
	err = GobDecode(encodeB, &decodeB)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println("decodeB:", decodeB)
}
