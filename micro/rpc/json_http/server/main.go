package main

import (
	"fmt"
	"io"
	"net/http"
	"net/rpc"
	"net/rpc/jsonrpc"

	"gitee.com/lishibao6/go-project-v2/micro/rpc/json_http/service"
)

var _ service.HelloService = (*HelloService)(nil)

type HelloService struct{}

func (s *HelloService) Greet(req string, resp *string) error {
	*resp = fmt.Sprintf("hello, %s", req)
	return nil
}

func NewRPCReadWriteCloserFromHTTP(w http.ResponseWriter, r *http.Request) *RPCReadWriteCloser {
	return &RPCReadWriteCloser{
		Writer:     w,
		ReadCloser: r.Body,
	}
}

type RPCReadWriteCloser struct {
	io.Writer
	io.ReadCloser
}

func main() {
	rpc.RegisterName("HelloService", &HelloService{})

	// RPC的服务假设在"/jsonrpc"路径
	// 在处理函数中基于http.ResponseWriter和http.Request类型的参数构造一个io.ReadWriteCloser类型的conn通道。
	// 然后基于conn构建针对服务端的json编码解码器。
	// 最后通过rpc.ServeRequest函数为每次请求处理一次RPC方法调用
	http.HandleFunc("/jsonrpc", func(w http.ResponseWriter, r *http.Request) {
		// reader: Read(p []byte) (n int, err error)
		// writer: Write(p []byte) (n int, err error)
		// closer: Close() error
		// r.Body.Close()

		// http http.ResponseWriter: Write([]byte) (int, error)
		// r.Body : Read(p []byte) (n int, err error)
		// r.Body :Close() error
		// 构造一个HTTP Conn
		conn := NewRPCReadWriteCloserFromHTTP(w, r)
		serverCodec := jsonrpc.NewServerCodec(conn)
		rpc.ServeRequest(serverCodec)
	})
	err := http.ListenAndServe(":1234", nil)
	if err != nil {
		panic(err)
	}
}

//go run server/main.go
