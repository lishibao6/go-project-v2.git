package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

type RPCRequest struct {
	Method string        `json:"method"`
	Params []interface{} `json:"params"`
	ID     int           `json:"id"`
}

func main() {
	// 构造 JSON-RPC 请求
	request := RPCRequest{
		Method: "HelloService.Greet",
		Params: []interface{}{"alice"},
		ID:     1,
	}

	// 将请求编码为 JSON 字节数组
	requestBody, err := json.Marshal(request)
	if err != nil {
		panic(err)
	}

	// 发送 HTTP POST 请求
	response, err := http.Post("http://localhost:1234/jsonrpc", "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()

	// 解码服务器返回的 JSON 响应
	var result struct {
		Result string `json:"result"`
		Error  struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
		} `json:"error"`
		ID int `json:"id"`
	}
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		panic(err)
	}

	// 检查服务器返回的结果
	if result.Error.Code != 0 {
		panic(fmt.Sprintf("RPC error %d: %s", result.Error.Code, result.Error.Message))
	}
	fmt.Println(result.Result)
}

// 方法一：go run client/main.go
// hello, alice

// 方法二：通过postman，详情请看../README.md
