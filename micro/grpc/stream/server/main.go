package main

import (
	"fmt"
	"io"
	"log"
	"net"

	"gitee.com/lishibao6/go-project-v2/micro/grpc/stream/pb"
	"gitee.com/lishibao6/go-project-v2/micro/grpc/stream/server/auth"
	"github.com/infraboard/mcube/logger/zap"
	"google.golang.org/grpc"
)

// 写一个接口的实现类
type helloServiceChannelServerImpl struct {
	pb.UnimplementedHelloServiceServer
}

// 实现流式响应
func (i *helloServiceChannelServerImpl) Channel(stream pb.HelloService_ChannelServer) error {
	for {
		// 1. 接收用户请求<也可以不处理>
		req, err := stream.Recv()
		if err != nil {
			// 如果遇到io.EOF表示客户端流被关闭
			if err == io.EOF {
				return nil
			}
			return err
		}

		// 2. 处理响应
		err = stream.Send(&pb.Response{Value: fmt.Sprintf("hello, %s", req.Value)})
		if err != nil {
			return err
		}
	}
}

func main() {
	zap.DevelopmentSetup()

	// 把我们实现了接口的对象HelloServiceServerImpl 注册给grpc框架

	// 1. 首先是通过grpc.NewServer()构造一个gRPC服务对象
	// 1.1 流拦截器
	streamServerInterceptor := auth.NewStreamServerInterceptorImpl().Auth
	grpcServerOption := grpc.ChainStreamInterceptor(streamServerInterceptor)
	grpcServer := grpc.NewServer(grpcServerOption)

	// 2. 把实现了接口的对象注册给grpc server
	// go-grpc插件，帮忙生成了对象的注册方法
	// 把HelloServiceServer的实现HelloServiceServerImpl注册给grpcServer
	pb.RegisterHelloServiceServer(grpcServer, &helloServiceChannelServerImpl{})

	// 3. 启动Grpc服务器, grpcServer是建立在HTTP2 网络之上的
	listener, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Fatal(err)
	}
	err = grpcServer.Serve(listener)
	if err != nil {
		panic(err)
	}
}

// stream $ go run server/main.go
// 2023-06-27 17:10:59     DEBUG   [middleware.auth]       auth/auth.go:68 admin 123456
