# grpc Stream

在当前hello目录执行: 初始化项目
```sh
go mod init "gitee.com/lishibao6/go-project-v2/micro/grpc/stream"
```


在当前hello目录执行:
```sh
protoc -I=. --go_out=./pb --go_opt=module="gitee.com/lishibao6/go-project-v2/micro/grpc/stream/pb" --go-grpc_out=./pb --go-grpc_opt=module="gitee.com/lishibao6/go-project-v2/micro/grpc/stream/pb" pb/stream.proto
```