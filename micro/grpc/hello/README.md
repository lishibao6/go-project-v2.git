# grpc hello world

在当前hello目录执行: 初始化项目
```sh
go mod init "gitee.com/lishibao6/go-project-v2/micro/grpc/hello"
```

在当前hello目录执行:
```sh
# protoc-gen-go
# --go_out=./pb --go_opt=module="gitee.com/lishibao6/go-project-v2/micro/grpc/hello/pb"
# protoc-gen-go-grpc
# --go-grpc_out=./pb --go-grpc_opt=module="gitee.com/lishibao6/go-project-v2/micro/grpc/hello/pb"
protoc -I=. --go_out=./pb --go_opt=module="gitee.com/lishibao6/go-project-v2/micro/grpc/hello/pb" --go-grpc_out=./pb --go-grpc_opt=module="gitee.com/lishibao6/go-project-v2/micro/grpc/hello/pb" pb/hello.proto
```