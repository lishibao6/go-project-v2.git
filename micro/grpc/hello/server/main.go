package main

import (
	"context"
	"fmt"
	"log"
	"net"

	"gitee.com/lishibao6/go-project-v2/micro/grpc/hello/pb"
	"gitee.com/lishibao6/go-project-v2/micro/grpc/hello/server/auth"
	"github.com/infraboard/mcube/logger/zap"
	"google.golang.org/grpc"
)

// 写一个对象，实现HelloServiceServer
type HelloServiceServerImpl struct {
	pb.UnimplementedHelloServiceServer
}

func (i *HelloServiceServerImpl) Greet(ctx context.Context, req *pb.Request) (*pb.Response, error) {
	return &pb.Response{Value: fmt.Sprintf("hello, %s", req.Value)}, nil
}

func main() {
	// 初始化全局Logger
	zap.DevelopmentSetup()

	// 如果把我们实现了接口的对象HelloServiceServerImpl 注册给grpc框架

	// 1. 首先是通过grpc.NewServer()构造一个gRPC服务对象
	// 1.1添加一个中间件
	// grpc.UnaryServerInterceptor(mid1)
	// 1.2 可以
	// grpc.ChainUnaryInterceptor(mid1, mid2, mid3)
	// 1.3 构造一个添加中间件的服务端参数, 实现一个auth中间件
	serverAuthInceptor := auth.NewServerAuthInterceptorImpl().Auth
	grpcServerOption := grpc.ChainUnaryInterceptor(serverAuthInceptor)
	grpcServer := grpc.NewServer(grpcServerOption)

	// 2. 把实现了接口的对象注册给grpc server
	// go-grpc插件，帮忙生成了对象的注册方法
	// 把HelloServiceServer的实现HelloServiceServerImpl注册给grpcServer
	pb.RegisterHelloServiceServer(grpcServer, &HelloServiceServerImpl{})

	// 3. 启动Grpc服务器, grpcServer是建立在HTTP2 网络之上的
	listener, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Fatal(err)
	}

	err = grpcServer.Serve(listener)
	if err != nil {
		panic(err)
	}
}

// $ go run server/main.go
// 2023-06-27 16:04:51     DEBUG   [middleware.auth]       auth/auth.go:52 req: value:"alice"  network address/p
// 2023-06-27 16:04:51     DEBUG   [middleware.auth]       auth/auth.go:53 server info: server: &{{}}, method: /hello.HelloService/Greet
// 2023-06-27 16:04:51     DEBUG   [middleware.auth]       auth/auth.go:64 admin 123456
// 2023-06-27 16:04:51     DEBUG   [middleware.auth]       auth/auth.go:73 resp: value:"hello,
// alice"
