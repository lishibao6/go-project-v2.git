package main

import (
	"context"
	"fmt"

	"gitee.com/lishibao6/go-project-v2/micro/grpc/hello/client/auth"
	"gitee.com/lishibao6/go-project-v2/micro/grpc/hello/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// 其他服务器上的客户端需要来调用grpc
func main() {
	// 1. 建立网络连接, 默认情况是http2是强制要求证书的
	// grpc.WithInsecure() 已经降级

	// 1.1客户端如何添加header, 在header认证信息
	// 1.2 客户端认证中间件参数 grpc.WithPerRPCCredentials(), 需要实现接口: credentials.PerRPCCredentials

	conn, err := grpc.Dial(
		"localhost:1234",
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithPerRPCCredentials(auth.NewPerRPCCredentialsImpl("admin", "123456")),
	)
	if err != nil {
		panic(err)
	}

	// 2. go-grpc插件已经生成好了grpc 的client sdk
	client := pb.NewHelloServiceClient(conn)

	// 3. 使用sdk
	// 添加认证信息
	// header := metadata.New(map[string]string{
	// 	auth.ClientHeaderKey: "admin",
	// 	auth.ClientSecretKey: "123456",
	// })
	resp, err := client.Greet(context.Background(), &pb.Request{Value: "alice"})
	if err != nil {
		panic(err)
	}
	fmt.Println(resp)
}

// hello $ go run client/main.go
// value:"hello, alice"
