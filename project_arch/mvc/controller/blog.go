package controller

import (
	"gitee.com/lishibao6/go-project-v2/project_arch/mvc/dao"
	"github.com/gin-gonic/gin"
)

func CreateBlog(ctx *gin.Context) {
	// 业务处理逻辑 也写在这里

	// 调用Dao完成数据的入库
	dao.SaveBlog()
}
