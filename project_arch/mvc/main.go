package main

import (
	"gitee.com/lishibao6/go-project-v2/project_arch/mvc/controller"
	"github.com/gin-gonic/gin"
)

// 工程入口
// 完成依赖的模块的拼装
func main() {
	r := gin.New()
	r.POST("/vlog/api/v1/blogs", controller.CreateBlog)
}
