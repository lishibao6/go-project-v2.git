package tag

import "context"

//tag接口
type Service interface {
	Query(context.Context, *QueryRequest) (*TagSet, error)
}

// 为什么要写这个构造函数
// 访问我们维护该对象的默认值
func NewQueryRequest(blogId int) *QueryRequest {
	return &QueryRequest{
		BlogId: blogId,
		Limit:  20,
	}
}

type QueryRequest struct {
	BlogId int
	Limit  int
}
