// 模拟的
package mock

import (
	"context"

	"gitee.com/lishibao6/go-project-v2/project_arch/ddd/tag"
)

func NewMock() *M {
	return &M{}
}

type M struct{}

func (m *M) Query(ctx context.Context, req *tag.QueryRequest) (*tag.TagSet, error) {
	return &tag.TagSet{
		Items: []*tag.Tag{
			{Key: "key1", Value: "value1"},
		},
	}, nil
}
