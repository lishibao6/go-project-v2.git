package tag

//Tag集合
type TagSet struct {
	Items []*Tag
}

//Tag结构体
type Tag struct {
	Key   string
	Value string
}
