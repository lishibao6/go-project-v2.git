package main

import (
	blogAPI "gitee.com/lishibao6/go-project-v2/project_arch/ddd/blog/http"
	"gitee.com/lishibao6/go-project-v2/project_arch/ddd/blog/impl"
	"gitee.com/lishibao6/go-project-v2/project_arch/ddd/tag/mock"
	"github.com/gin-gonic/gin"
)

func main() {
	// 把业务路由通过Web框架对外提供服务
	r := gin.Default()

	// 这个时候依赖的tag实现是 mock, mock对象, 当tag模块的服务开发完成后, 传递具体的实现

	//Tag Service的Mock实现
	tagServiceImpl := mock.NewMock()

	// blogServiceImpl 依赖 Tag服务的实现
	blogServiceImpl := impl.NewImpl(tagServiceImpl)

	// 实例化Blog Service Impl对象
	blogHandler := blogAPI.NewHandler(blogServiceImpl)

	// 注册模块需要对外暴露的HTTP方法
	blogHandler.Registry(r)

	// 提供RestFul API服务
	r.Run("localhost:80")
}
