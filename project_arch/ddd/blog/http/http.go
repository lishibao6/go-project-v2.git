package http

import (
	"gitee.com/lishibao6/go-project-v2/project_arch/ddd/blog"
	"github.com/gin-gonic/gin"
)

func NewHandler(svc blog.Service) *Handler {
	return &Handler{
		service: svc,
	}
}

type Handler struct {
	// 这里的实现, 可以是具体实现, 也可以是mock对象
	// 也可以任意切换具体的实现 基于MongoDB/MySQL/ES/Etcd/Hbase
	service blog.Service
}

// 把该业务的handler注册给根路由
func (h *Handler) Registry(r gin.IRouter) {
	r.POST("/vblog/api/v1/blogs", h.CreateBlog)
}

// http 协议逻辑处理
func (h *Handler) CreateBlog(ctx *gin.Context) {
	h.service.CreateBlog(nil, nil)
}
