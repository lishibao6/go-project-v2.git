// 具体的实现
package impl

import (
	"context"

	"gitee.com/lishibao6/go-project-v2/project_arch/ddd/blog"
	"gitee.com/lishibao6/go-project-v2/project_arch/ddd/tag"
)

func NewImpl(tag tag.Service) *Impl {
	return &Impl{
		tag: tag,
	}
}

type Impl struct {
	tag tag.Service
}

func (i *Impl) CreateBlog(ctx context.Context, req *blog.CreateBlogRequest) (*blog.Blog, error) {

	//如何和Tag模块进行交互
	queryRequest := tag.NewQueryRequest(1)
	i.tag.Query(ctx, queryRequest)
	return nil, nil
}
